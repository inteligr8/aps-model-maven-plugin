/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.alfresco.activiti.ApsPublicRestApiJerseyImpl;

/**
 * A class that implements an APS service download goal.
 * 
 * This goal will simply download the APS App with the specified name from the
 * specified APS service.  The downloaded APS App will remain packed (zipped).
 * If a file exists with a conflicting name, it will be overwritten.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "download-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class DownloadAppGoal extends ApsAppAddressibleGoal {

	@Parameter( property = "aps-model.download.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File zipDirectory;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.validateTargetDirectory();
		
		Long appId = this.findAppModel(true).getId();
		File appZip = this.downloadApp(appId);
		
		File toAppZip = new File(this.zipDirectory, this.apsAppName + ".zip");
		if (toAppZip.exists())
			toAppZip.delete();
		
		try {
			FileUtils.copyFile(appZip, toAppZip);
		} catch (IOException ie) {
			throw new MojoFailureException("The downloaded APS App could not be saved", ie);
		}
	}
	
	protected void validateTargetDirectory() {
		if (!this.zipDirectory.exists()) {
			this.getLog().debug("Creating APS Apps directory: " + this.zipDirectory);
			this.zipDirectory.mkdirs();
		} else if (!this.zipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'zipDirectory' refers to a file and not a directory");
		}
	}
	
	private File downloadApp(long appId) throws MojoExecutionException {
		ApsPublicRestApiJerseyImpl api = this.getApsApi();
		this.getLog().debug("Downloading APS App: " + appId);
		return api.getAppDefinitionsApi().export(appId);
	}

}

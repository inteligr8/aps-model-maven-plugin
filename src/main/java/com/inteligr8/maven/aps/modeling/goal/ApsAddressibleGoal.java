/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.crypto.DefaultSettingsDecryptionRequest;
import org.apache.maven.settings.crypto.SettingsDecrypter;

import com.inteligr8.alfresco.activiti.ApsClientJerseyConfiguration;
import com.inteligr8.alfresco.activiti.ApsClientJerseyImpl;
import com.inteligr8.alfresco.activiti.ApsPublicRestApiJerseyImpl;
import com.inteligr8.alfresco.activiti.model.Tenant;

/**
 * This class adds APS addressbility to extending goals.
 * 
 * This is a common need for several goals in this library, as most of those
 * goals will need to call an APS service to perform their work.
 * 
 * @author brian@inteligr8.com
 */
public abstract class ApsAddressibleGoal extends DisablableGoal {
	
	@Parameter( defaultValue = "${session}", readonly = true )
	protected MavenSession session;
	
	@Parameter( property = "aps-model.baseUrl", required = true, defaultValue = "http://localhost:8080/activiti-app"  )
	protected String activitiAppBaseUrl;
	
	@Parameter( property = "aps-model.authType", required = true, defaultValue = "BASIC" )
	protected String activitiAppAuthType;
	
	@Parameter( property = "aps-model.basicAuth.mavenServerId", required = true, defaultValue = "aps" )
	protected String activitiAppAuthBasicServerId;
	
	@Parameter( property = "aps-model.oauth.code", required = false )
	protected String oauthCode;
	
	@Parameter( property = "aps-model.oauth.client.mavenServerId", required = false )
	protected String oauthClientServerId;
	
	@Parameter( property = "aps-model.oauth.mavenServerId", required = false )
	protected String oauthServerId;
	
	@Parameter( property = "aps-model.oauth.tokenUrl", required = false )
	protected String oauthTokenUrl;
    
    @Component
    private SettingsDecrypter decrypter;
	
	private ApsPublicRestApiJerseyImpl api;
	
	/**
	 * Retrieves an APS client configuration.
	 * 
	 * The configuration is built based on the properties injected into this
	 * class.  It supports either `BASIC` or `OAuth` based authentication.
	 * 
	 * @return An APS client configuration.
	 */
	public ApsClientJerseyConfiguration getApsClientConfiguration() throws MojoExecutionException {
		this.getLog().debug("Configuring APS to URL: " + this.activitiAppBaseUrl);
		ApsClientJerseyConfiguration config = new ApsClientJerseyConfiguration();
		config.setBaseUrl(this.activitiAppBaseUrl);
		
		switch (this.activitiAppAuthType.toUpperCase()) {
			case "BASIC":
				this.getLog().info("Configuring APS with BASIC authentication");

				Server creds = this.session.getSettings().getServer(this.activitiAppAuthBasicServerId);
				if (this.activitiAppAuthBasicServerId != null && creds == null)
					this.getLog().warn("The Maven configuration has no server '" + this.activitiAppAuthBasicServerId + "'; continuing with default credentials");
				
				if (creds != null) {
				    creds = this.decrypter.decrypt(new DefaultSettingsDecryptionRequest(creds))
				            .getServer();
				    
					this.getLog().debug("Username: " + creds.getUsername());
					config.setBasicAuthUsername(creds.getUsername());
					config.setBasicAuthPassword(creds.getPassword());
				}
				break;
			case "OAUTH":
				this.getLog().info("Configuring APS with OAuth authentication");

				Server clientCreds = this.session.getSettings().getServer(this.oauthClientServerId);
				Server oauthCreds = this.session.getSettings().getServer(this.oauthServerId);
				if ((this.oauthClientServerId != null || this.oauthServerId != null) && clientCreds == null && oauthCreds == null)
					this.getLog().warn("The Maven configuration has no server '" + this.oauthClientServerId + "' or '" + this.oauthServerId + "'; continuing without credentials");
				
				if (this.oauthCode != null) {
    				this.getLog().debug("OAuth Code: " + this.oauthCode);
    				config.setOAuthAuthCode(this.oauthCode);
				}
				
				if (clientCreds != null) {
                    creds = this.decrypter.decrypt(new DefaultSettingsDecryptionRequest(clientCreds))
                            .getServer();
                    
					this.getLog().debug("OAuth Client ID: " + clientCreds.getUsername());
					config.setOAuthClientId(clientCreds.getUsername());
					if (clientCreds.getPassword() != null && clientCreds.getPassword().length() > 0)
					    config.setOAuthClientSecret(clientCreds.getPassword());
				}
				
				if (oauthCreds != null) {
                    creds = this.decrypter.decrypt(new DefaultSettingsDecryptionRequest(oauthCreds))
                            .getServer();
                    
					this.getLog().debug("OAuth Username: " + oauthCreds.getUsername());
					config.setOAuthUsername(oauthCreds.getUsername());
                    if (oauthCreds.getPassword() != null && oauthCreds.getPassword().length() > 0)
                        config.setOAuthPassword(oauthCreds.getPassword());
				}
				
				config.setOAuthTokenUrl(this.oauthTokenUrl);
				break;
			default:
				throw new IllegalArgumentException("The 'activitiAppAuthType' configuration must be either 'Basic' or 'OAuth'");
		}
		
		return config;
	}
	
	/**
	 * This method constructs and caches the APS API accessor.
	 * 
	 * @return An APS API instance.
     * @throws MojoExecutionException The APS API failed to initialize.
	 */
	public synchronized ApsPublicRestApiJerseyImpl getApsApi() throws MojoExecutionException {
		if (this.api == null) {
			ApsClientJerseyConfiguration config = this.getApsClientConfiguration();
			ApsClientJerseyImpl apsClient = new ApsClientJerseyImpl(config);
			this.api = new ApsPublicRestApiJerseyImpl(apsClient);
		}
		
		return this.api;
	}
    
    /**
     * This method makes the appropriate service calls to find the first APS
     * tenant ID from the configured APS service.
     * 
     * This method does not cache the result.
     * 
     * @return An APS tenant ID; null only if there are no tenants.
     * @throws MojoExecutionException The APS API failed to initialize.
     */
    protected Long findTenantId() throws MojoExecutionException {
        List<Tenant> tenants = this.getApsApi().getAdminApi().getTenants();
        if (tenants == null || tenants.isEmpty())
            return null;
        return tenants.iterator().next().getId();
    }

}

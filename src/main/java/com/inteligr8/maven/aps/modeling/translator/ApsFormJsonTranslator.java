/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.maven.aps.modeling.util.Index;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS Form JSON configuration file translator.
 * 
 * This will translate the form ID embedded into APS Form descriptor.
 * 
 * @author brian@inteligr8.com
 */
public class ApsFormJsonTranslator implements ApsFileTranslator {

	private final Logger logger = LoggerFactory.getLogger(ApsFormJsonTranslator.class);
	private final Index<Long, String> apsIndex;
	
	/**
	 * This constructor initializes the default translator.
	 * 
	 * @param apsFormIndex A map of form IDs to form names as defined in the APS Service.
	 */
	public ApsFormJsonTranslator(Index<Long, String> apsFormIndex) {
		this.apsIndex = apsFormIndex;
	}
	
	@Override
	public void translateFile(File file, String formName, Long formId) throws IOException {
		this.logger.debug("Translating JSON file: {}", file);
		boolean changed = false;
		
		ObjectNode jsonDescriptor = ModelUtil.getInstance().readJson(file, ObjectNode.class);
		JsonNode jsonResourceId = jsonDescriptor.get("resourceId");
		if (jsonResourceId == null || jsonResourceId.isNull()) {
			this.logger.debug("The form has no ID to translate");
			return;
		}
		
		this.logger.trace("Found ID {} in the '{}' APS Form descriptor", jsonResourceId, formName);
		
		Long apsFormId = this.apsIndex.getFirstKey(formName);
		if (apsFormId == null) {
			this.logger.debug("The form '{}' does not exist in APS; leaving unchanged", formName);
		} else if (!formId.equals(apsFormId)) {
			this.logger.debug("The form '{}' exists in APS with ID {}; changing descriptor", formName, apsFormId);
			jsonDescriptor.put("resourceId", apsFormId);
			changed = true;
		} else {
			this.logger.trace("The form '{}' ID does not change; leaving unchanged", formName, apsFormId);
		}
		
		if (changed)
			ModelUtil.getInstance().writeJson(jsonDescriptor, file, false);
	}

}

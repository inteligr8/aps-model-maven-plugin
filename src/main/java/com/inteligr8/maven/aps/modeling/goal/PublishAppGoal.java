/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.alfresco.activiti.ApsPublicRestApiJerseyImpl;
import com.inteligr8.alfresco.activiti.model.AppDefinitionPublishRepresentation;
import com.inteligr8.alfresco.activiti.model.AppDefinitionUpdateResultRepresentation;

/**
 * A class that implements an APS Service publish goal.
 * 
 * This goal will simply attempt to publish an existing APS App using the APS
 * Service.  It is typically used in conjuection with the `upload-app` goal.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "publish-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class PublishAppGoal extends ApsAppAddressibleGoal {

	@Parameter( property = "aps-model.publish.comment", required = true, defaultValue = "Automated by 'aps-model-maven-plugin'" )
	protected String comment;

	@Parameter( property = "aps-model.dryRun", required = true, defaultValue = "false" )
	protected boolean dryRun;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		Long appId = this.findAppModel(false).getId();
		
		try {
			this.publishApp(appId);
		} catch (IOException ie) {
			throw new MojoFailureException("The APS App could not be published", ie);
		}
	}
	
	private void publishApp(Long appId) throws IOException, MojoExecutionException {
		ApsPublicRestApiJerseyImpl api = this.getApsApi();
		
		AppDefinitionPublishRepresentation appDefPublish = new AppDefinitionPublishRepresentation();
		appDefPublish.setComment(this.comment);
		if (this.dryRun) {
			this.getLog().info("[DRYRUN]: Publishing app: " + appId);
		} else {
			this.getLog().info("Publishing app: " + appId);
			AppDefinitionUpdateResultRepresentation response = api.getAppDefinitionsApi().publish(appId, appDefPublish);
			if (Boolean.TRUE.equals(response.getError()))
			    throw new WebApplicationException(response.getErrorDescription(), Response.Status.PRECONDITION_FAILED);
		}
	}

}

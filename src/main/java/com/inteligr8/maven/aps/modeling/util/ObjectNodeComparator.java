/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This class implements a comparator for the value of JSON object fields.
 * 
 * If field values are considered equal, fallback fields are supported.  Any
 * object without the field in question will be considered "greater than" ones
 * that have the specified field.  This will move them to the end of any sorted
 * list.
 * 
 * If a JSON value or array is encountered instead of a JSON object, then it is
 * treated exactly like a JSON object without the specified field.  It will be
 * considered "greater than" and move to the end of any sorted list.
 * 
 * @author brian@inteligr8.com
 */
public class ObjectNodeComparator implements Comparator<JsonNode> {
	
	//private final Logger logger = LoggerFactory.getLogger(ArrayOfObjectNodeComparator.class);
	private final List<String> objectFieldNames;
	
	/**
	 * An array-based constructor.
	 * 
	 * The class will sort on the values of these JSON object field names, with
	 * a priority in the specified order.
	 * 
	 * @param objectFieldNames An array of JSON object field names.
	 */
	public ObjectNodeComparator(String... objectFieldNames) {
		this(Arrays.asList(objectFieldNames));
	}
	
	/**
	 * A list-based constructor.
	 * 
	 * The class will sort on the values of these JSON object field names, with
	 * a priority in the specified order.
	 * 
	 * @param objectFieldNames A list of JSON object field names.
	 */
	public ObjectNodeComparator(List<String> objectFieldNames) {
		this.objectFieldNames = objectFieldNames;
	}
	
	@Override
	public int compare(JsonNode o1, JsonNode o2) {
		if (!o1.isObject())
			return 1;
		if (!o2.isObject())
			return -1;
		
		for (String objectFieldName : objectFieldNames) {
			JsonNode value1 = o1.get(objectFieldName);
			JsonNode value2 = o2.get(objectFieldName);
			
			if (value1 == null && value2 == null)
				continue;
			
			if (value1 == null)
				return 1;
			if (value2 == null)
				return -1;
			if (!value1.getNodeType().equals(value2.getNodeType()))
				return 0;
			
			switch (value1.getNodeType()) {
				case BOOLEAN:
					return Boolean.compare(value1.asBoolean(), value2.asBoolean());
				case NUMBER:
					return Double.compare(value1.asDouble(), value2.asDouble());
				case STRING:
					return value1.asText().compareTo(value2.asText());
				default:
					return 0;
			}
		}
		
		return 0;
	}

}

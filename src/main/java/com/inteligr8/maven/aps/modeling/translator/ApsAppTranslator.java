/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.activiti.model.Datum;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.api.ModelsApi.ModelType;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.alfresco.activiti.model.ModelRepresentation;
import com.inteligr8.alfresco.activiti.model.ResultListDataRepresentation;
import com.inteligr8.alfresco.activiti.model.Tenant;
import com.inteligr8.maven.aps.modeling.crawler.ApsAppCrawlable;
import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;
import com.inteligr8.maven.aps.modeling.util.Index;

/**
 * This class defines an APS App package translator.
 * 
 * An APS App package is a ZIP file that contains multiple files in a
 * predictable folder hierachy with predictable file names.  It is effectively
 * an APS App interchange format specification.
 * 
 * A package must have at least a single configuration file at the root of the
 * ZIP package in the JSON format.  It must be named the APS App name.  That
 * file will then reference all the model elements contained in the ZIP.  Any
 * model elements not referenced will simply be ignored by APS and by this
 * plugin.
 * 
 * This class has methods that provide translator for all the various model
 * elements in an APS App package.
 * 
 * @author brian@inteligr8.com
 */
public class ApsAppTranslator implements ApsAppCrawlable {

	private final Logger logger = LoggerFactory.getLogger(ApsAppTranslator.class);
	private final Pattern filenamePattern = Pattern.compile("([^/]+)-([0-9]+)\\.(json|bpmn20\\.xml)");
	private final ApsPublicRestApi api;
	private final File appDirectory;
	
	private boolean indexesBuilt = false;
	private Map<String, GroupLight> apsOrgIndex;
	private Index<Long, String> apsFormIndex;
	private Index<Long, String> apsProcessIndex;
	private Index<Long, String> fileFormIndex;
	private Index<Long, String> fileProcessIndex;
	private Index<Long, String> fileSubprocessIndex;
	
	/**
	 * This constructor initializes the default translator.
	 * 
	 * The specified directory and a reference to the APS Service are both
	 * required.  This way the configuration file can be cross-referenced with
	 * the APS App IDs on the target APS instance.
	 * 
	 * @param apsAppDirectory The directory of the unpacked APS package.
	 * @param api An APS API reference.
	 */
	public ApsAppTranslator(File apsAppDirectory, ApsPublicRestApi api) {
		this.api = api;
		this.appDirectory = apsAppDirectory;
	}
	
	/**
	 * This method initializes the data required from the APS Service for the
	 * function of this class.
	 *  
	 * @throws IOException A network I/O related issue occurred.
	 */
	public synchronized void buildIndexes() throws IOException {
		if (this.indexesBuilt)
			return;
		
		this.logger.info("Building indexes ...");
		
		long tenantId = this.findTenantId();
		this.logger.debug("APS App tenant ID: {}", tenantId);
		
		this.apsOrgIndex = this.buildApsGroupMap(tenantId);
		this.logLarge("APS App organizations: {}", this.apsOrgIndex);
		
		this.apsProcessIndex = this.buildApsModelIndex(ModelType.Process);
		this.logLarge("APS process models: {}", this.apsProcessIndex);
		
		this.apsFormIndex = this.buildApsModelIndex(ModelType.Form);
		this.logLarge("APS form models: {}", this.apsFormIndex);
		
		this.fileFormIndex = this.buildFileIndex("form-models", this.apsFormIndex, true);
		this.logLarge("File form models: {}", this.fileFormIndex);
		
		this.fileProcessIndex = this.buildFileIndex("bpmn-models", this.apsProcessIndex, true);
		this.logLarge("File process models: {}", this.fileProcessIndex);
		
		this.fileSubprocessIndex = this.buildFileIndex("bpmn-subprocess-models", this.apsProcessIndex, true);
		this.logLarge("File subprocess models: {}", this.fileSubprocessIndex);
		
		this.indexesBuilt = true;
	}
	
	private <K, V> void logLarge(String message, Map<K, V> map) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(message, map);
		} else {
			this.logger.debug(message, map.keySet());
		}	
	}
	
	private <K, V> void logLarge(String message, Index<K, V> index) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(message, index);
		} else {
			this.logger.debug(message, index.valueSet());
		}
	}
	
	@Override
	public ApsFileTransformer getAppJsonTransformer() {
		if (!this.indexesBuilt)
			throw new IllegalStateException("The indexes are never built");
		
		return new ApsAppJsonTranslator(
				this.api,
				this.apsProcessIndex,
				this.apsOrgIndex,
				this.fileProcessIndex);
	}
	
	@Override
	public ApsFileTransformer getFormJsonTransformer() {
		if (!this.indexesBuilt)
			throw new IllegalStateException("The indexes are never built");
		
		return new ApsFormJsonTranslator(
				this.apsFormIndex);
	}
	
	@Override
	public ApsFileTransformer getProcessJsonTransformer() {
		if (!this.indexesBuilt)
			throw new IllegalStateException("The indexes are never built");
		
		return new ApsProcessJsonTranslator(
				this.api,
				this.apsProcessIndex,
				this.apsOrgIndex,
				this.apsFormIndex);
	}
	
	@Override
	public ApsFileTransformer getProcessBpmnTransformer() {
		if (!this.indexesBuilt)
			throw new IllegalStateException("The indexes are never built");
		
		return new ApsProcessBpmnTranslator(
				this.api,
				this.apsProcessIndex,
				this.apsOrgIndex,
				this.apsFormIndex,
				this.fileFormIndex);
	}
	
	protected Long findTenantId() {
		List<Tenant> tenants = this.api.getAdminApi().getTenants();
		return (tenants == null || tenants.isEmpty()) ? null : tenants.iterator().next().getId();
	}
	
	protected Map<String, GroupLight> buildApsGroupMap(long tenantId) {
		List<GroupLight> groups = this.api.getAdminApi().getGroups(tenantId, true, true);

		Map<String, GroupLight> map = new HashMap<>(groups.size());
		for (GroupLight group : groups)
			map.put(group.getName(), group);
		return map;
	}
	
    protected Index<Long, String> buildApsModelIndex(ModelType modelType) {
		ResultList<ModelRepresentation> results = this.api.getModelsApi().get("everyone", null, modelType.getId(), null);
		this.logger.debug("APS {} models found: {} out of {}", modelType, results.getSize(), results.getTotal());

		Index<Long, String> map = new Index<>(results.getSize().intValue(), false);
		
		try {
			for (ModelRepresentation model : results.getData()) {
				map.put(model.getId(), model.getName());
				
				// FIXME add paging support
			}
		} catch (IllegalArgumentException iae) {
			throw new IllegalStateException("This goal does not support the current state of APS", iae);
		}

		return map;
	}
	
	private Index<Long, String> buildFileIndex(String modelFolderName, Index<Long, String> apsProcessIndex, boolean renameFiles) {
		File subprocessesDirectory = new File(this.appDirectory, modelFolderName);
		return this.buildFileIndex(subprocessesDirectory, apsProcessIndex, renameFiles);
	}
	
	protected Index<Long, String> buildFileIndex(File modelDirectory, Index<Long, String> apsModelIndex, boolean renameFiles) {
		if (!modelDirectory.exists()) {
			this.logger.debug("APS model directory doesn't exist; creating empty index: {}", modelDirectory);
			return new Index<>(0, false);
		}
		
		if (!modelDirectory.isDirectory())
			throw new IllegalStateException("The APS model directory is actually a file: " + modelDirectory);
		
		Index<Long, String> fileModelIndex = new Index<>(apsModelIndex.size(), false);
		
		for (File modelFile : modelDirectory.listFiles()) {
			this.logger.trace("Building index for model file: {}", modelFile);
			
			Matcher matcher = this.filenamePattern.matcher(modelFile.getName());
			String modelName = matcher.replaceFirst("$1");
			String modelIdStr = matcher.replaceFirst("$2");
			String modelExt = matcher.replaceFirst("$3");
			long fileModelId = Long.parseLong(modelIdStr);
			this.logger.trace("Building index for model {} ID: {}", modelName, fileModelId);
			
			fileModelIndex.put(fileModelId, modelName);
			
			Set<Long> apsModelIds = apsModelIndex.getKeys(modelName);
			if (apsModelIds == null || apsModelIds.isEmpty()) {
				this.logger.debug("The model file '{}' has no corresponding model in APS; treating as new", modelFile.getName());
				continue;
			}
			
			if (apsModelIds.size() > 1)
				throw new IllegalStateException("An APS " + modelDirectory.getName() + " name was used multiple times: " + modelName);
			
			if (renameFiles) {
				Long apsModelId = apsModelIds.iterator().next();
				if (apsModelId != fileModelId) {
					this.logger.debug("Renaming form model from '" + modelFile.getName() + "' to match APS form ID: " + apsModelId);
					File newModelFile = new File(modelFile.getParentFile(), modelName + "-" + apsModelId + "." + modelExt);
					modelFile.renameTo(newModelFile);
				} else {
					this.logger.debug("The APS model '{}' and corresponding model file '{}' have matching IDs", modelName, modelFile.getName());
				}
			}
		}
		
		return fileModelIndex;
	}

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.alfresco.activiti.ApsPublicRestApiJerseyImpl;
import com.inteligr8.alfresco.activiti.model.AppDefinitionUpdateResultRepresentation;
import com.inteligr8.alfresco.activiti.model.FileMultipartJersey;
import com.inteligr8.alfresco.activiti.model.ModelRepresentation;

/**
 * A class that implements an APS service upload goal.
 * 
 * This goal will simply upload an APS App package with the specified name to
 * the specified APS service.  Any IDs specified in the uploaded package must
 * match existing IDs for them to properly version.  That is the main purpose
 * of this plugin and can be achieved using the 'translate-app' goal.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "upload-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class UploadAppGoal extends ApsAppAddressibleGoal {

	@Parameter( property = "aps-model.upload.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File zipDirectory;
	
	@Parameter( property = "publish", required = true, defaultValue = "false" )
	protected boolean publish = false;

	@Parameter( property = "aps-model.dryRun", required = true, defaultValue = "false" )
	protected boolean dryRun;
	
	protected final int bufferSize = 128 * 1024;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		File sourceFile = this.validateSourceDirectory();
		
		ModelRepresentation appModel = this.findAppModel(false);
		
		try {
			this.uploadApp(appModel, sourceFile);
		} catch (IOException ie) {
			throw new MojoFailureException("The APS App could not be uploaded", ie);
		}
	}
	
	protected File validateSourceDirectory() {
		if (!this.zipDirectory.exists()) {
			throw new IllegalStateException("The 'zipDirectory' does not exist: " + this.zipDirectory);
		} else if (!this.zipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'zipDirectory' is not a directory: " + this.zipDirectory);
		}
		
		File sourceFile = new File(this.zipDirectory, this.apsAppName + ".zip");
		
		if (!sourceFile.exists()) {
			throw new IllegalStateException("The App file does not exists in the 'zipDirectory': " + sourceFile);
		} else if (!sourceFile.isFile()) {
			throw new IllegalStateException("The App file is not a file: " + sourceFile);
		}
		
		return sourceFile;
	}
	
	private void uploadApp(ModelRepresentation appModel, File appZip) throws IOException, MojoExecutionException, MojoFailureException {
		ApsPublicRestApiJerseyImpl api = this.getApsApi();
		
		FileInputStream fistream = new FileInputStream(appZip);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			FileMultipartJersey multipart = FileMultipartJersey.from(appZip.getName(), bistream);
			
			if (appModel == null) {
				if (this.publish) {
					if (this.dryRun) {
						this.getLog().info("[DRYRUN]: Uploading & publishing new APS App: " + this.apsAppName);
					} else {
						this.getLog().info("Uploading & publishing new APS App: " + this.apsAppName);
						AppDefinitionUpdateResultRepresentation appDefUpdate = api.getAppDefinitionsJerseyApi().publishApp(multipart);
						if (Boolean.TRUE.equals(appDefUpdate.getError()))
							throw new WebApplicationException(appDefUpdate.getErrorDescription(), Response.Status.PRECONDITION_FAILED);
					}
				} else {
					if (this.dryRun) {
						this.getLog().info("[DRYRUN]: Uploading new APS App: " + this.apsAppName);
					} else {
						this.getLog().info("Uploading new APS App: " + this.apsAppName);
						api.getAppDefinitionsJerseyApi().importApp(multipart, true);
					}
				}
			} else {
				if (this.publish) {
					if (this.dryRun) {
						this.getLog().info("[DRYRUN]: Uploading, versioning, & publishing APS App: " + this.apsAppName + " (" + appModel.getId() + ")");
					} else {
						this.getLog().info("Uploading, versioning, & publishing APS App: " + this.apsAppName + " (" + appModel.getId() + ")");
						AppDefinitionUpdateResultRepresentation appDefUpdate = api.getAppDefinitionsJerseyApi().publishApp(appModel.getId(), multipart);
						if (Boolean.TRUE.equals(appDefUpdate.getError()))
                            throw new WebApplicationException(appDefUpdate.getErrorDescription(), Response.Status.PRECONDITION_FAILED);
					}
				} else {
					if (this.dryRun) {
						this.getLog().info("[DRYRUN]: Uploading & versioning APS App: " + this.apsAppName + " (" + appModel.getId() + ")");
					} else {
						this.getLog().info("Uploading & versioning APS App: " + this.apsAppName + " (" + appModel.getId() + ")");
						api.getAppDefinitionsJerseyApi().importApp(appModel.getId(), multipart, true);
					}
				}
			}
		} catch (ParseException pe) {
			throw new MojoExecutionException("The app ZIP could not be wrapped in a multipart", pe);
		} finally {
			bistream.close();
		}
	}

}

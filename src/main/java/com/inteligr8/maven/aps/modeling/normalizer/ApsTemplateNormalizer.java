/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;
import com.inteligr8.maven.aps.modeling.crawler.ApsTemplateCrawlable;

/**
 * This class defines an APS template normalizer.
 * 
 * @author brian@inteligr8.com
 */
public class ApsTemplateNormalizer implements ApsTemplateCrawlable {

	private final boolean enableSorting;
	
	/**
	 * This constructor initializes the default normalizer with or without
	 * sorting.
	 * 
	 * The sorting feature is available for a better "diff" experience.  If
	 * you intend to commit the APS template configurations to Git, you will
	 * want to enable sorting.
	 * 
	 * @param enableSorting true to re-order JSON objects; false to keep as-is.
	 */
	public ApsTemplateNormalizer(boolean enableSorting) {
		this.enableSorting = enableSorting;
	}
	
	@Override
	public ApsFileTransformer getTemplateJsonTransformer() {
		return new ApsTemplateJsonNormalizer(this.enableSorting);
	}

}

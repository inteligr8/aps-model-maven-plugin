/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * This class adds enable/disable capability to any goal.  It uses the standard
 * 'skip' terminology.
 */
public abstract class DisablableGoal extends AbstractMojo {
	
	@Parameter( property = "aps-model.skip", required = true, defaultValue = "false" )
	protected boolean skip = false;
	
	protected boolean isEnabled() {
		return !this.skip;
	}
	
	protected boolean isDisabled() {
		return this.skip;
	}
	
	@Override
	public final void execute() throws MojoExecutionException, MojoFailureException {
		if (this.isEnabled())
			this.executeEnabled();
	}
	
	/**
	 * Identical to the `AbstractMojo.execute()` method, but it is only called
	 * when this is enabled.
	 * 
	 * @see AbstractMojo#execute()
	 * 
	 * @throws MojoExecutionException The goal failed to execute.
	 * @throws MojoFailureException The goal failed whiling executing.
	 */
	public abstract void executeEnabled() throws MojoExecutionException, MojoFailureException;

}

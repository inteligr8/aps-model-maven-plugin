/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.inteligr8.maven.aps.modeling.xml.DomNamespaceContext;

/**
 * This singleton class provides JSON and XML I/O tools and consistent formatting.
 * 
 * @author brian@inteligr8.com
 */
public class ModelUtil {
	
	public final static QName CDATA_FLEX = QName.valueOf("inteligr8:xml:cdata_or_element");
	private final static ModelUtil INSTANCE = new ModelUtil();
	
	/**
	 * @return A singleton instance of this class.
	 */
	public static ModelUtil getInstance() {
		return INSTANCE;
	}
	
	
	
	private final ObjectMapper om = new ObjectMapper().registerModule(new JavaTimeModule());
	private final ObjectMapper omsorted = new ObjectMapper().registerModule(new JavaTimeModule());
	private final DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
	private final DocumentBuilder docBuilder;
	private final XPathFactory xpathfactory = XPathFactory.newInstance();
	private final TransformerFactory tfactory = TransformerFactory.newInstance();
	private final Transformer transformer;
	private final int bufferSize = 128 * 1024;
	
	private ModelUtil() {
		this.dbfactory.setNamespaceAware(true);
		
		try {
			this.docBuilder = this.dbfactory.newDocumentBuilder();
			
			InputStream istream = this.getClass().getResourceAsStream("/bpmn.xslt");
			BufferedInputStream bistream = new BufferedInputStream(istream, this.bufferSize);
			try {
				StreamSource ssource = new StreamSource(bistream);
				this.transformer = this.tfactory.newTransformer(ssource);
			} finally {
				bistream.close();
			}
		} catch (ParserConfigurationException | TransformerConfigurationException | IOException e) {
			throw new ExceptionInInitializerError(e);
		}
		
		// makes Git handling a whole lot better
		this.om.enable(SerializationFeature.INDENT_OUTPUT);
		this.omsorted.enable(SerializationFeature.INDENT_OUTPUT);

		// makes Git handling better between environment nuances
		this.omsorted.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
		this.omsorted.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
	}
	
	
	
	/**
	 * This method reads/parses JSON and immediately formats/writes JSON back
	 * to the specified file.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * This method will load the full set of JSON objects into memory.
	 * 
	 * @param inplaceFile A file handle to an existing valid JSON file.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void json(File inplaceFile, boolean enableSorting) throws IOException {
		JsonNode json = this.readJson(inplaceFile);
		this.writeJson(json, inplaceFile, enableSorting);
	}
	
	/**
	 * This method reads/parses JSON from the specified source file and
	 * immediately formats/writes JSON to the specified target file.
	 * 
	 * If the specified target file already exists, it will be overwritten.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * This method will load the full set of JSON objects into memory.
	 * 
	 * @param sourceFile A file handle to an existing valid JSON file.
	 * @param targetFile A file handle for the target JSON file.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void json(File sourceFile, File targetFile, boolean enableSorting) throws IOException {
		FileInputStream fistream = new FileInputStream(sourceFile);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			FileOutputStream fostream = new FileOutputStream(targetFile);
			BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
			try {
				this.json(bistream, bostream, enableSorting);
			} finally {
				bostream.close();
			}
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses JSON from the specified source stream and
	 * immediately formats/writes JSON to the specified target stream.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * This method will load the full set of JSON objects into memory.
	 * 
	 * @param istream An input stream to valid JSON.
	 * @param ostream An output stream.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A stream I/O issue occurred.
	 */
	public void json(InputStream istream, OutputStream ostream, boolean enableSorting) throws IOException {
		JsonNode json = this.readJson(istream);
		this.writeJson(json, ostream, enableSorting);
	}
	
	/**
	 * This method reads/parses JSON from the specified file.
	 * 
	 * @param file A file handle to an existing valid JSON file.
	 * @return A JSON node (array, object, or value).
	 * @throws IOException A file I/O issue occurred.
	 */
	public JsonNode readJson(File file) throws IOException {
		return this.readJson(file, JsonNode.class);
	}
	
	/**
	 * This method reads/parses JSON from the specified file.
	 * 
	 * @param <T> The class of the type to parse.
	 * @param file A file handle to an existing valid JSON file.
	 * @param type A type parse the JSON into (using the Jackson parser).
	 * @return An instance of the specified type.
	 * @throws IOException A file I/O issue occurred.
	 */
	public <T> T readJson(File file, Class<T> type) throws IOException {
		FileInputStream fistream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			return this.readJson(bistream, type);
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses JSON from the specified file.
	 * 
	 * @param <T> The class of the type to parse.
	 * @param file A file handle to an existing valid JSON file.
	 * @return A JSON array as a list.
	 * @throws IOException A file I/O issue occurred.
	 */
	public <T> List<T> readJsonAsList(File file) throws IOException {
		FileInputStream fistream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			return this.readJsonAsList(bistream);
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses JSON from the specified file.
	 * 
	 * @param file A file handle to an existing valid JSON file.
	 * @return A JSON object as a map.
	 * @throws IOException A file I/O issue occurred.
	 */
	public Map<String, Object> readJsonAsMap(File file) throws IOException {
		FileInputStream fistream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			return this.readJsonAsMap(bistream);
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses JSON from the specified stream.
	 * 
	 * @param istream An input stream to valid JSON.
	 * @return A JSON node (array, object, or value).
	 * @throws IOException A stream I/O issue occurred.
	 */
	public JsonNode readJson(InputStream istream) throws IOException {
		return this.om.readTree(istream);
	}
	
	/**
	 * This method reads/parses JSON from the specified stream.
	 * 
	 * @param <T> The class of the type to parse.
	 * @param istream An input stream to valid JSON.
	 * @param type A type parse the JSON into (using the Jackson parser).
	 * @return An instance of the specified type.
	 * @throws IOException A stream I/O issue occurred.
	 */
	public <T> T readJson(InputStream istream, Class<T> type) throws IOException {
		return this.om.readValue(istream, type);
	}
	
	/**
	 * This method reads/parses JSON from the specified stream.
	 * 
	 * @param <T> The class of the type to parse.
	 * @param istream An input stream to valid JSON.
	 * @return A JSON array as a list.
	 * @throws IOException A stream I/O issue occurred.
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> readJsonAsList(InputStream istream) throws IOException {
		return this.om.readValue(istream, List.class);
	}
	
	/**
	 * This method reads/parses JSON from the specified stream.
	 * 
	 * @param istream An input stream to valid JSON.
	 * @return A JSON object as a map.
	 * @throws IOException A stream I/O issue occurred.
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> readJsonAsMap(InputStream istream) throws IOException {
		return this.om.readValue(istream, Map.class);
	}
    
    /**
     * This method reads/parses a Java POJO.
     * 
     * @param o A Java POJO.
     * @return A JSON node (array, object, or value).
     */
    public ObjectNode readPojo(Object o) {
        return this.om.convertValue(o, ObjectNode.class);
    }
    
    /**
     * This method reads/parses a Java POJO.
     * 
     * @param o A Java POJO.
     * @return A Java POJO as a map.
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> readPojoAsMap(Object o) {
        return this.om.convertValue(o, Map.class);
    }
	
	/**
	 * This method formats/writes JSON to the specified file.
	 * 
	 * If the specified file already exists, it will be overwritten.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * @param json A JSON node (array, object, or value).
	 * @param file A file handle.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void writeJson(JsonNode json, File file, boolean enableSorting) throws IOException {
		FileOutputStream fostream = new FileOutputStream(file);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
		try {
			this.writeJson(json, bostream, enableSorting);
		} finally {
			bostream.close();
		}
	}
	
	/**
	 * This method formats/writes JSON to the specified file.
	 * 
	 * If the specified file already exists, it will be overwritten.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * @param map A Java map.
	 * @param file A file handle.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void writeJson(Map<String, Object> map, File file, boolean enableSorting) throws IOException {
		FileOutputStream fostream = new FileOutputStream(file);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
		try {
			this.writeJson(map, bostream, enableSorting);
		} finally {
			bostream.close();
		}
	}
	
	/**
	 * This method formats/writes JSON to the specified file.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * @param json A JSON node (array, object, or value).
	 * @param ostream An output stream.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void writeJson(JsonNode json, OutputStream ostream, boolean enableSorting) throws IOException {
		if (enableSorting) {
			this.omsorted.writeValue(ostream, json);
		} else {
			this.om.writeValue(ostream, json);
		}
	}
	
	/**
	 * This method formats/writes JSON to the specified file.
	 * 
	 * You many optionally enable the sorting of elements.  Sorting makes the
	 * files more diff-friendly.  The algorithm will sort JSON objects by keys.
	 * JSON array values will not be sorted.
	 * 
	 * @param map A Java map.
	 * @param ostream An output stream.
	 * @param enableSorting true to sort; false to leave as-is.
	 * @throws IOException A file I/O issue occurred.
	 */
	public void writeJson(Map<String, Object> map, OutputStream ostream, boolean enableSorting) throws IOException {
		if (enableSorting) {
			this.omsorted.writeValue(ostream, map);
		} else {
			this.om.writeValue(ostream, map);
		}
	}
    
    /**
     * This method formats/writes a Java POJO of the specified type using the
     * specified map.
     * 
     * @param <T> The class of the type to create.
     * @param map A Java map.
     * @param type A Java class to create.
     * @return A Java POJO instance.
     */
    public <T> T writePojo(Map<String, Object> map, Class<T> type) {
        return this.om.convertValue(map, type);
    }
	

	
	/**
	 * This method reads/parses XML and immediately formats/writes XML back to
	 * the specified file.
	 * 
	 * This method will load the full DOM of XML into memory.
	 * 
	 * @param inplaceFile A file handle to an existing valid XML file.
	 * @throws IOException A file I/O issue occurred.
	 * @throws SAXException The file is not valid XML.
	 * @throws TransformerException An unknown XML transformation issue occurred.
	 */
	public void xml(File inplaceFile) throws IOException, SAXException, TransformerException {
		Document xml;
		
		FileInputStream fistream = new FileInputStream(inplaceFile);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			xml = this.readXml(bistream);
		} finally {
			bistream.close();
		}

		FileOutputStream fostream = new FileOutputStream(inplaceFile);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
		try {
			this.writeXml(xml, bostream);
		} finally {
			bostream.close();
		}
	}
	
	/**
	 * This method reads/parses XML from the specified source file and
	 * immediately formats/writes XML to to the specified target file.
	 * 
	 * If the specified target file already exists, it will be overwritten.
	 * 
	 * This method will load the full DOM of XML into memory.
	 * 
	 * @param sourceFile A file handle to an existing valid XML file.
	 * @param targetFile A file handle.
	 * @throws IOException A file I/O issue occurred.
	 * @throws SAXException The file is not valid XML.
	 * @throws TransformerException An unknown XML transformation issue occurred.
	 */
	public void xml(File sourceFile, File targetFile) throws IOException, SAXException, TransformerException {
		FileInputStream fistream = new FileInputStream(sourceFile);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			FileOutputStream fostream = new FileOutputStream(targetFile);
			BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
			try {
				this.xml(bistream, bostream);
			} finally {
				bostream.close();
			}
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses XML from the specified source stream and
	 * immediately formats/writes XML to to the specified target stream.
	 * 
	 * This method will load the full DOM of XML into memory.
	 * 
	 * @param istream An input stream to valid XML.
	 * @param ostream An output stream.
	 * @throws IOException A stream I/O issue occurred.
	 * @throws SAXException The stream is not valid XML.
	 * @throws TransformerException An unknown XML transformation issue occurred.
	 */
	public void xml(InputStream istream, OutputStream ostream) throws IOException, SAXException, TransformerException {
		Document doc = this.readXml(istream);
		this.writeXml(doc, ostream);
	}
	
	/**
	 * This method reads/parses XML from the specified file.
	 * 
	 * @param file A file handle to an existing valid XML file.
	 * @return An XML DOM Document object.
	 * @throws IOException A file I/O issue occurred.
	 * @throws SAXException The file is not valid XML.
	 */
	public Document readXml(File file) throws IOException, SAXException {
		FileInputStream fistream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			return this.readXml(bistream);
		} finally {
			bistream.close();
		}
	}
	
	/**
	 * This method reads/parses XML from the specified stream.
	 * 
	 * @param istream An input stream to valid XML.
	 * @return An XML DOM Document object.
	 * @throws IOException A stream I/O issue occurred.
	 * @throws SAXException The stream is not valid XML.
	 */
	public Document readXml(InputStream istream) throws IOException, SAXException {
		return this.docBuilder.parse(istream);
	}
	
	/**
	 * This method formats/writes XML to the specified file.
	 * 
	 * If the specified file already exists, it will be overwritten.
	 * 
	 * @param xml An XML element.
	 * @param file A file handle.
	 * @throws IOException A file I/O issue occurred.
	 * @throws TransformerException An unknown XML transformation issue occurred.
	 */
	public void writeXml(Node xml, File file) throws IOException, TransformerException {
		FileOutputStream fostream = new FileOutputStream(file);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
		try {
			this.writeXml(xml, bostream);
		} finally {
			bostream.close();
		}
	}
	
	/**
	 * This method formats/writes XML to the specified stream.
	 * 
	 * @param xml An XML element.
	 * @param ostream An output stream.
	 * @throws IOException A stream I/O issue occurred.
	 * @throws TransformerException An unknown XML transformation issue occurred.
	 */
	public void writeXml(Node xml, OutputStream ostream) throws IOException, TransformerException {
		DOMSource source = new DOMSource(xml);
		StreamResult result = new StreamResult(ostream);
		this.transformer.transform(source, result);
	}


	
	/**
	 * This method searches the DOM using an XPath expression.
	 * 
	 * @param node An XML node.
	 * @param xpathExpr An XPath expression.
	 * @return An XML DOM list of nodes object; null if no result.
	 * @throws XPathExpressionException An unknown XPath issue occurred.
	 */
	public NodeList xpath(Node node, String xpathExpr) throws XPathExpressionException {
		return (NodeList)this.xpath(node, xpathExpr, XPathConstants.NODESET);
	}
	
	/**
	 * This method searches the DOM using an XPath expression.
	 * 
	 * @param node An XML node.
	 * @param xpathExpr An XPath expression.
	 * @param returnType An XPath return type: see XPathConstants.
	 * @return An XML DOM list of nodes object; null if no result.
	 * @throws XPathExpressionException An unknown XPath issue occurred.
	 */
	public Object xpath(Node node, String xpathExpr, QName returnType) throws XPathExpressionException {
		XPath xpath = this.xpathfactory.newXPath();
		xpath.setNamespaceContext(new DomNamespaceContext(node));
		
		if (returnType.equals(ModelUtil.CDATA_FLEX)) {
			NodeList nodes = (NodeList)xpath.evaluate(xpathExpr, node, XPathConstants.NODESET);
			Node anode = this.getFirstCDataSectionOrLastElement(nodes);
			if (anode instanceof CharacterData) {
				// nothing special
			} else if (anode instanceof Element) {
				Node anode2 = this.getFirstCDataSectionOrLastElement(anode.getChildNodes());
				if (anode2 == null)
					return null;
				if (!(anode2 instanceof CharacterData))
					throw new IllegalStateException();
				anode = anode2;
			} else {
				return null;
			}

			return anode;
		} else {
			return xpath.evaluate(xpathExpr, node, returnType);
		}
	}
	
	private Node getFirstCDataSectionOrLastElement(NodeList nodes) {
		Element element = null;
		
		for (int n = 0; n < nodes.getLength(); n++) {
			Node anode = nodes.item(n);
			if (anode instanceof CharacterData) {
				return (CharacterData)anode;
			} else if (anode.getNodeType() == Node.ELEMENT_NODE) {
				element = (Element)anode;
			}
		}
		
		return element;
	}

}

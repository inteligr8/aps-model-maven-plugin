/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.maven.aps.modeling.util.Index;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS template JSON configuration file translator.
 * 
 * @author brian@inteligr8.com
 */
public class ApsTemplateJsonTranslator implements ApsFileTranslator {

	private final Logger logger = LoggerFactory.getLogger(ApsTemplateJsonTranslator.class);
	private final Index<Long, String> apsDocumentTemplateIndex;
	private final Index<Long, String> apsCustomEmailTemplateIndex;
	
	/**
	 * This constructor initializes the default translator.
	 */
	public ApsTemplateJsonTranslator(
			Index<Long, String> apsDocumentTemplateIndex,
			Index<Long, String> apsCustomEmailTemplateIndex) {
		this.apsDocumentTemplateIndex = apsDocumentTemplateIndex;
		this.apsCustomEmailTemplateIndex = apsCustomEmailTemplateIndex;
	}
	
	@Override
	public void translateFile(File file, String _unsued1, Long _unused2) throws IOException {
		this.logger.debug("Translating JSON file: {}", file);

		boolean changed = false;

		ObjectNode json = (ObjectNode) ModelUtil.getInstance().readJson(file);
		
		boolean isDocumentTemplate = json.get("mimeType") != null;
		String templateName = json.get("name").asText();
        this.logger.trace("Found template name '{}' in APS template file: {}", templateName, file);
        
        Long oldTemplateId = null;
        if (json.hasNonNull("id")) {
            oldTemplateId = json.get("id").asLong();
            this.logger.trace("Found template ID '{}' in APS template file: {}", oldTemplateId, file);
        }
        
		Long newTemplateId = null;
		if (isDocumentTemplate) {
		    newTemplateId = this.apsDocumentTemplateIndex.getFirstKey(templateName);
		} else {
            newTemplateId = this.apsCustomEmailTemplateIndex.getFirstKey(templateName);
		}
		
		if (newTemplateId == null) {
		    // new template; remove the key completely
		    json.remove("id");
		    changed = true;
		} else if (newTemplateId.equals(oldTemplateId)) {
		    // unchanged; nothing to do
		} else {
		    json.put("id", newTemplateId);
            changed = true;
		}
		
		if (changed)
			ModelUtil.getInstance().writeJson(json, file, false);
	}

}

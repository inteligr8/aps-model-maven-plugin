/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.xml;

import java.util.Collections;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

/**
 * This class implements namespace handling for DOM processing.
 */
public class DomNamespaceContext implements NamespaceContext {
	
	private final Logger logger = LoggerFactory.getLogger(DomNamespaceContext.class);
	private final Node node;
	private final String defaultNamespacePrefix;
	private final String defaultNamespace;
	
	/**
	 * The constructor using just a DOM node.  The default namespace prefix is
	 * assumed to be 'tns'.
	 * 
	 * @param node A DOM node.
	 */
	public DomNamespaceContext(Node node) {
		this(node, "tns");
	}
	
	/**
	 * The constructor using just a DOM node.
	 * 
	 * @param node A DOM node.
	 * @param defaultNamespacePrefix A DOM namespace prefix.
	 */
	public DomNamespaceContext(Node node, String defaultNamespacePrefix) {
		this.node = node;
		this.defaultNamespacePrefix = defaultNamespacePrefix;

		String defaultNamespace = null;
		
		while (node != null && defaultNamespace == null) {
			defaultNamespace = node.lookupNamespaceURI(null);
			node = node.getParentNode();
		}
		
		this.defaultNamespace = defaultNamespace;
	}
	
	@Override
	public String getNamespaceURI(String prefix) {
		this.logger.trace("Performing namespace lookup by prefix: {}", prefix);
		
		if (prefix.equals(this.defaultNamespacePrefix))
			return this.defaultNamespace;
		
		Node node = this.node;
		String namespace = null;
		
		while (node != null && namespace == null) {
			namespace = node.lookupNamespaceURI(prefix);
			node = node.getParentNode();
		}
		
		return namespace;
	}
	
	@Override
	public String getPrefix(String namespaceURI) {
		this.logger.trace("Performing prefix lookup by namespace: {}", namespaceURI);
		
		Node node = this.node;
		String prefix = null;
		
		while (node != null && prefix == null) {
			prefix = node.lookupPrefix(namespaceURI);
			node = node.getParentNode();
		}

		if (namespaceURI.equals(this.defaultNamespace))
			return this.defaultNamespacePrefix;
		
		return prefix;
	}
	
	@Override
	public Iterator<String> getPrefixes(String namespaceURI) {
		String prefix = this.getPrefix(namespaceURI);
		return prefix == null ? Collections.emptyListIterator() : Collections.singletonList(prefix).iterator();
	}

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.crawler;

import java.io.File;
import java.io.IOException;

/**
 * This interface provides a common method for transforming various files in an
 * exported APS App.
 * 
 * @author brian@inteligr8.com
 */
public interface ApsFileTransformer {

	/**
	 * This method transforms the specified file which should be referred to
	 * with the specified name and ID.
	 * 
	 * @param file A model file.
	 * @param modelName The target model name.
	 * @param modelId The target model ID.
	 * @throws IOException An I/O related exception has occurred during transformation.
	 */
	void transformFile(File file, String modelName, Long modelId) throws IOException;

}

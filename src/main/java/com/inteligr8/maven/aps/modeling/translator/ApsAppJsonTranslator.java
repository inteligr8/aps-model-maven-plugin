/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.maven.aps.modeling.util.Index;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS App JSON configuration file translator.
 * 
 * This will translate the IDs of all the APS App, Processes, and Organizations
 * referenced in the APS App descriptor.
 * 
 * Caution: in lieu of implementing APS User translation, APS User identity
 * references are simply removed.  Using APS Users in your APS models is bad
 * practice.  Use APS Organizations, even if that user is the only member of
 * the group.
 * 
 * @author brian@inteligr8.com
 */
public class ApsAppJsonTranslator extends ApsOrganizationHandler implements ApsFileTranslator {

	private final Logger logger = LoggerFactory.getLogger(ApsAppJsonTranslator.class);
	private final Map<String, GroupLight> apsOrgIndex;
	private final Index<Long, String> apsProcessIndex;
	private final Index<Long, String> fileProcessIndex;
	
	/**
	 * This constructor initializes the default translator.
	 * 
	 * @param api An APS API reference.
	 * @param apsProcessIndex A map of process IDs to process names as defined in the APS Service.
	 * @param apsOrgIndex A map of organizations (groups) to organization meta-data as defined in the APS Service.
	 * @param fileProcessIndex A map of process IDs to process names as used in the local process files.
	 */
	public ApsAppJsonTranslator(
			ApsPublicRestApi api,
			Index<Long, String> apsProcessIndex,
			Map<String, GroupLight> apsOrgIndex,
			Index<Long, String> fileProcessIndex) {
		super(api, apsOrgIndex);
		this.apsProcessIndex = apsProcessIndex;
		this.apsOrgIndex = apsOrgIndex;
		this.fileProcessIndex = fileProcessIndex;
	}
	
	@Override
	public void translateFile(File file, String appName, Long appId) throws IOException {
		this.logger.debug("Translating JSON file: {}", file);

		boolean changed = false;

		JsonNode jsonDescriptor = ModelUtil.getInstance().readJson(file);
		
		changed = this.translateModels(jsonDescriptor, appName) || changed;
		changed = this.translateIdentities(jsonDescriptor, appName) || changed;
		
		if (changed)
			ModelUtil.getInstance().writeJson(jsonDescriptor, file, false);
	}
	
	private boolean translateModels(JsonNode jsonDescriptor, String appName) {
		this.logger.trace("Translating processes in App: {}", appName);
		boolean changed = false;
		
		for (JsonNode _jsonModel : jsonDescriptor.get("definition").get("models")) {
			ObjectNode jsonModel = (ObjectNode)_jsonModel;
			
			String fileProcessName = jsonModel.get("name").asText();
			this.logger.trace("Found process '{}' in APS App descriptor", fileProcessName);
			
			if (this.apsProcessIndex.containsValue(fileProcessName)) {
				long fileProcessId = jsonModel.get("id").asLong();
				this.logger.debug("The process '{}' has ID {} in APS App descriptor", fileProcessName, fileProcessId);
				
				if (!fileProcessName.equals(this.fileProcessIndex.getValue(fileProcessId))) {
					throw new IllegalArgumentException("DATA INTEGRITY: The APS App descriptor and BPMN proess descriptor are mismatched");
				} else if (this.fileProcessIndex.getKeys(fileProcessName).isEmpty()) {
					throw new IllegalArgumentException("DATA INTEGRITY: The APS App descriptor references a missing BPMN process file");
				}

				Long apsProcessId = this.apsProcessIndex.getFirstKey(fileProcessName);
				if (apsProcessId == null) {
					this.logger.debug("The process '{}' is not found in APS; leaving unchanged", fileProcessName);
				} else if (apsProcessId != fileProcessId) {
					this.logger.debug("The process '{}' is found in APS with ID {}; changing descriptor", fileProcessName, apsProcessId);
					jsonModel.put("id", apsProcessId);
					changed = true;
				} else {
					this.logger.trace("The process '{}' ID does not change; leaving unchanged", fileProcessName);
				}
			}
			
			if (jsonModel.has("version")) {
				jsonModel.put("version", 0);
				changed = true;
			}
//			int fields = jsonModel.size();
//			jsonModel.remove(Arrays.asList("version"));
//			if (jsonModel.size() < fields)
//				changed = true;
		}
		
		return changed;
	}
	
	private boolean translateIdentities(JsonNode jsonDescriptor, String appName) {
		this.logger.trace("Translating identities in App: {}", appName);
		boolean changed = false;
		
		Iterator<JsonNode> i = jsonDescriptor.get("definition").get("publishIdentityInfo").iterator();
		while (i.hasNext()) {
			JsonNode jsonIdentityInfo = i.next();
			
			String identityType = jsonIdentityInfo.get("type").asText();
			if (identityType == null) {
				this.logger.warn("Found identity with no type; ignoring ...");
				continue;
			}
			
			switch (identityType.toLowerCase()) {
				case "user":
					this.logger.warn("Found 'user' identity, which should never be used for portablility; removing ...");
					changed = true;
					i.remove();
					break;
				case "group":
					changed = this.translateGroup(jsonIdentityInfo) || changed;
					break;
				default:
					this.logger.warn("Found '{}' identity, which is not expected; ignoring ...", identityType);
			}
		}
		
		return changed;
	}
	
	private boolean translateGroup(JsonNode jsonIdentityInfo) {
		boolean changed = false;
		ObjectNode jsonGroup = (ObjectNode)jsonIdentityInfo.get("group");
		
		String fileOrgName = jsonGroup.get("name").asText();
		this.logger.trace("Found organization '{}' in APS App descriptor", fileOrgName);
		
		if (this.apsOrgIndex.containsKey(fileOrgName)) {
			long fileOrgId = jsonGroup.get("id").asLong();
			GroupLight apsOrg = this.apsOrgIndex.get(fileOrgName);
			
			if (apsOrg == null) {
				this.logger.debug("The organization '{}' does not exist in APS; adding to APS", fileOrgName);
				long apsGroupId = this.createOrganization(fileOrgName);
				jsonGroup.put("id", apsGroupId);
				changed = true;
			} else if (fileOrgId != apsOrg.getId()) {
				this.logger.debug("The organization '{}' exists in APS with ID {}; changing descriptor", fileOrgName, apsOrg.getId());
				jsonGroup.put("id", apsOrg.getId());
				changed = true;
			} else {
				this.logger.trace("The organization '{}' ID does not change; leaving unchanged", fileOrgName);
			}

			if (jsonGroup.has("parentGroupId")) {
				jsonGroup.put("parentGroupId", 0);
				changed = true;
			}
//			if (jsonGroup.has("parentGroupId")) {
//				jsonGroup.remove("parentGroupId");
//				changed = true;
//			}
		} else {
			this.logger.warn("The organization '{}' does not exist in APS", fileOrgName);
			// FIXME create the organization
		}
		
		return changed;
	}

}

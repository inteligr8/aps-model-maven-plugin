/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class that implements bi-directional map for forward and reverse indexing
 * and lookups.
 * 
 * The index is considered to be many-to-1.  So the forward index appears as
 * 1-to-1 (one key to one value) and the reverse index appears as 1-to-many
 * (one value to many keys).
 * 
 * If the index should be considered to be 1-to-1, then set
 * `allowDuplicateValues` to `false`.
 * 
 * @author brian@inteligr8.com
 * @param <K> An index key type.
 * @param <V> An index value type.
 */
public class Index<K, V> {
	
	private final Map<K, V> forwardMap;
	private final Map<V, Set<K>> reverseMap;
	private final boolean allowDuplicateValues;
	
	/**
	 * This constructor defines a new index with some meta-data.
	 * 
	 * The capacity is not strictly enforced.  It should be accurate for
	 * performance reasons.
	 * 
	 * @param initialCapacity A capacity, as you would specify in an ArrayList or HashMap.
	 * @param allowDuplicateValues true to allow multiple keys to have the same value; false to throw an IllegalArgumentException.
	 */
	public Index(int initialCapacity, boolean allowDuplicateValues) {
		this.forwardMap = new HashMap<K, V>(initialCapacity);
		this.reverseMap = new HashMap<V, Set<K>>(initialCapacity);
		this.allowDuplicateValues = allowDuplicateValues;
	}
	
	/**
	 * @param key An index key.
	 * @return true if it exists; false otherwise.
	 */
	public boolean containsKey(K key) {
		return this.forwardMap.containsKey(key);
	}
	
	/**
	 * @param value An indexed value.
	 * @return true if it exists; false otherwise.
	 */
	public boolean containsValue(V value) {
		return this.reverseMap.containsKey(value);
	}
	
	/**
	 * @param key An index key.
	 * @return The value; null if key not indexed.
	 */
	public V getValue(K key) {
		return this.forwardMap.get(key);
	}
	
	/**
	 * @return A set of all keys; may be empty; never null.
	 */
	public Set<K> keySet() {
		return this.forwardMap.keySet();
	}
	
	/**
	 * @return A set of all values; may be empty; never null.
	 */
	public Set<V> valueSet() {
		return this.reverseMap.keySet();
	}
	
	/**
	 * @param value An indexed value.
	 * @return A set of keys; never empty; null if value not indexed.
	 */
	public Set<K> getKeys(V value) {
		return this.reverseMap.get(value);
	}
	
	/**
	 * @param value An indexed value.
	 * @return The key; null if value not indexed.
	 */
	public K getFirstKey(V value) {
		Set<K> keys = this.reverseMap.get(value);
		return (keys == null || keys.isEmpty()) ? null : keys.iterator().next();
	}
	
	/**
	 * @return The number of keys in the index.
	 */
	public int size() {
		return this.forwardMap.size();
	}
	
	/**
	 * @return Interable access to the entries of the forward index.
	 */
	public Iterable<Entry<K, V>> entries() {
		return this.forwardMap.entrySet();
	}
	
	/**
	 * @param key An index key.
	 * @param value A value to index to the specified key.
	 * @return true if the key already existed and was overwritten; false otherwise.
	 */
	public synchronized boolean put(K key, V value) {
		if (key == null)
			throw new IllegalArgumentException("An index key may not be null");
		if (value == null)
			throw new IllegalArgumentException("An index value may not be null");
		
		boolean overwrote = false;
		
		V oldValue = this.forwardMap.get(key);
		if (oldValue != null) {
			this.reverseMap.get(oldValue)
					.remove(key);
			overwrote = true;
		}
		
		this.forwardMap.put(key, value);
		
		Set<K> keys = this.reverseMap.get(value);
		if (keys == null) {
			this.reverseMap.put(value, keys = new HashSet<>(2));
		} else if (this.allowDuplicateValues && !keys.isEmpty() && !key.equals(keys.iterator().next())) {
			throw new IllegalArgumentException("The map explicitly disallows duplicate values.  The value '" + value + "' is already indexed by: " + keys);
		}
		keys.add(key);
		
		return overwrote;
	}
	
	/**
	 * @param key An index key.
	 * @return true if the key and its value were removed; false otherwise.
	 */
	public synchronized boolean remove(K key) {
		if (key == null)
			throw new IllegalArgumentException("An index key may not be null");
		
		V oldValue = this.forwardMap.remove(key);
		if (oldValue == null)
			return false;
		
		this.reverseMap.get(oldValue)
				.remove(key);
		return true;
	}
	
	/**
	 * This method clears/resets the index.
	 */
	public synchronized void clear() {
		this.forwardMap.clear();
		this.reverseMap.clear();
	}
	
	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this.forwardMap);
		} catch (JsonProcessingException jpe) {
			return "\"JSON processing error\"";
		}
	}

}

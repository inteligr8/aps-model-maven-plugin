/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS Process BPMN/XML configuration file normalizer.
 * 
 * This will remove the 'exportDateTime' and 'modelLastUpdated' dates and
 * 'modelVersion' of the process model.
 * 
 * @author brian@inteligr8.com
 */
public class ApsProcessBpmnNormalizer implements ApsFileNormalizer {

	private static final String NAMESPACE_ACTIVITI_MODELER = "http://activiti.com/modeler";
	
	private final Logger logger = LoggerFactory.getLogger(ApsProcessBpmnNormalizer.class);
	
	@Override
	public void normalizeFile(File file, String modelName) throws IOException {
		this.logger.debug("Normalizing Process BPMN file: {}", file);
		
		boolean changed = false;
		
		try {
			Document bpmn = ModelUtil.getInstance().readXml(file);
			Element definitionsElement = bpmn.getDocumentElement();

			changed = this.removeAttributeIfSet(definitionsElement, NAMESPACE_ACTIVITI_MODELER, "exportDateTime") || changed;
			changed = this.removeAttributeIfSet(definitionsElement, NAMESPACE_ACTIVITI_MODELER, "modelLastUpdated") || changed;
			changed = this.removeAttributeIfSet(definitionsElement, NAMESPACE_ACTIVITI_MODELER, "modelVersion") || changed;
			//changed = this.setAttributeIfSet(definitionsElement, NAMESPACE_ACTIVITI_MODELER, "modelVersion", "0") || changed;
			
			if (changed)
				ModelUtil.getInstance().writeXml(bpmn, file);
		} catch (TransformerException | SAXException e) {
			throw new IllegalStateException("An XML issue occurred", e);
		}
	}
	
	private boolean removeAttributeIfSet(Element element, String attrNamespace, String attrName) {
		Attr attr = element.getAttributeNodeNS(attrNamespace, attrName);
		if (attr == null)
			return false;
		
		element.removeAttributeNode(attr);
		return true;
	}

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.maven.aps.modeling.util.Index;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS Process JSON configuration file translator.
 * 
 * This will translate the IDs of all the APS Process, Organizations, and
 * Forms referenced in the APS Process descriptor.
 * 
 * Caution: in lieu of implementing APS User translation, APS User identity
 * references are simply removed.  Using APS Users in your APS models is bad
 * practice.  Use APS Organizations, even if that user is the only member of
 * the group.
 * 
 * @author brian@inteligr8.com
 */
public class ApsProcessJsonTranslator extends ApsOrganizationHandler implements ApsFileTranslator {

	private final Logger logger = LoggerFactory.getLogger(ApsProcessJsonTranslator.class);
	private final Index<Long, String> apsIndex;
	private final Map<String, GroupLight> apsOrgIndex;
	private final Index<Long, String> apsFormIndex;
	
	/**
	 * This constructor initializes the default translator.
	 * 
	 * @param api An APS API reference.
	 * @param apsProcessIndex A map of process IDs to process names as defined in the APS Service.
	 * @param apsOrgIndex A map of organizations (groups) to organization meta-data as defined in the APS Service.
	 * @param apsFormIndex A map of form IDs to form names as defined in the APS Service.
	 */
	public ApsProcessJsonTranslator(
			ApsPublicRestApi api,
			Index<Long, String> apsProcessIndex,
			Map<String, GroupLight> apsOrgIndex,
			Index<Long, String> apsFormIndex) {
		super(api, apsOrgIndex);
		this.apsIndex = apsProcessIndex;
		this.apsOrgIndex = apsOrgIndex;
		this.apsFormIndex = apsFormIndex;
	}
	
	@Override
	public void translateFile(File file, String processName, Long processId) throws IOException {
		this.logger.debug("Translating JSON file: {}", file);
		
		boolean changed = false;
		Long apsProcessId = this.apsIndex.getFirstKey(processName);
		
		ObjectNode jsonDescriptor = ModelUtil.getInstance().readJson(file, ObjectNode.class);
		
		changed = this.translateResourceId(jsonDescriptor, processName, apsProcessId) || changed;
		changed = this.translateOrganizations(jsonDescriptor, processName) || changed;
		changed = this.translateForms(jsonDescriptor, processName) || changed;
		changed = this.translateSubprocesses(jsonDescriptor, processName) || changed;
		
		if (changed)
			ModelUtil.getInstance().writeJson(jsonDescriptor, file, false);
	}
	
	private boolean translateResourceId(ObjectNode jsonDescriptor, String processName, Long apsProcessId) {
		if (apsProcessId == null) {
			this.logger.trace("The process is not in APS; treating as new");
			return false;
		}
		
		this.logger.trace("Translating resource ID in process: {}", processName);
		ObjectNode resourceIdParentJson = jsonDescriptor;
		
		JsonNode jsonResourceId = jsonDescriptor.get("resourceId");
		if (jsonResourceId == null || jsonResourceId.isNull()) {
			JsonNode jsonEditorJson = jsonDescriptor.get("editorJson");
			if (jsonEditorJson != null && jsonEditorJson.isObject()) {
				resourceIdParentJson = (ObjectNode)jsonEditorJson;
				jsonResourceId = jsonEditorJson.get("resourceId");
			}
		}

		if (jsonResourceId == null || jsonResourceId.isNull()) {
			this.logger.trace("The process 'resourceId' is not defined; leaving unchanged", apsProcessId);
			return false;
		} else if (jsonResourceId.asLong() != apsProcessId) {
			this.logger.debug("The process is found in APS with ID {}; changing 'resourceId' in descriptor", apsProcessId);
			resourceIdParentJson.put("resourceId", apsProcessId);
			return true;
		} else {
			this.logger.trace("The process ID {} does not change; leaving unchanged", apsProcessId);
			return false;
		}
	}
	
	
	
	private boolean translateOrganizations(ObjectNode jsonDescriptor, String processName) {
		this.logger.trace("Translating organizations in process: {}", processName);
		boolean changed = false;
		
		for (JsonNode jsonChildShape : this.getChildShapes(jsonDescriptor)) {
			try {
				JsonNode jsonUserTaskAssignment = (JsonNode)jsonChildShape.get("properties").get("usertaskassignment");
				if (!jsonUserTaskAssignment.isObject())
					continue;
				
				for (JsonNode _jsonCandidateGroup : jsonUserTaskAssignment.get("assignment").get("idm").get("candidateGroups")) {
					ObjectNode jsonCandidateGroup = (ObjectNode)_jsonCandidateGroup;
					changed = this.translateOrganization(jsonCandidateGroup) || changed;
				}
			} catch (NullPointerException npe) {
				// suppress; gracefully skip
			}
		}
		
		return changed;
	}
	
	private boolean translateOrganization(ObjectNode jsonCandidateGroup) {
		String fileOrgName = jsonCandidateGroup.get("name").asText();
		this.logger.trace("Found organization '{}' in the APS Process descriptor", fileOrgName);
		
		if (this.apsOrgIndex.containsKey(fileOrgName)) {
			long fileOrgId = jsonCandidateGroup.get("id").asLong();
			GroupLight apsOrg = this.apsOrgIndex.get(fileOrgName);
			
			if (apsOrg == null) {
				this.logger.debug("The organization '{}' does not exist in APS; adding to APS", fileOrgName);
				long apsGroupId = this.createOrganization(fileOrgName);
				jsonCandidateGroup.put("id", apsGroupId);
				return true;
			} else if (apsOrg.getId() != fileOrgId) {
				this.logger.debug("The organization '{}' exists in APS with ID {}; changing descriptor", fileOrgName, apsOrg.getId());
				jsonCandidateGroup.put("id", apsOrg.getId());
				return true;
			} else {
				this.logger.trace("The organization '{}' ID does not change; leaving unchanged", fileOrgName);
				return false;
			}
		} else {
			// FIXME automatically add the group?
			this.logger.debug("The organization '{}' does not exist in APS; leaving unchanged", fileOrgName);
			return false;
		}
	}
	
	
	
	private boolean translateForms(ObjectNode jsonDescriptor, String processName) {
		this.logger.trace("Translating forms in process: {}", processName);
		boolean changed = false;
		
		for (JsonNode jsonChildShape : this.getChildShapes(jsonDescriptor)) {
			try {
				JsonNode jsonConditionalSequenceFlow = (JsonNode)jsonChildShape.get("properties").get("conditionsequenceflow");
				changed = this.translateConditionSequenceFlow(jsonConditionalSequenceFlow) || changed;

				JsonNode jsonFormRef = (JsonNode)jsonChildShape.get("properties").get("formreference");
				changed = this.translateReference(jsonFormRef, this.apsFormIndex) || changed;
			} catch (NullPointerException npe) {
				// suppress; gracefully skip
			}
		}
		
		return changed;
	}
	
	private boolean translateConditionSequenceFlow(JsonNode jsonConditionalSequenceFlow) {
		if (jsonConditionalSequenceFlow == null || !jsonConditionalSequenceFlow.isObject())
			return false;

		this.logger.trace("Found a conditional sequence flow in the APS Process descriptor");

        boolean changed = false;
        
		ObjectNode expression = (ObjectNode)jsonConditionalSequenceFlow.get("expression");
		while (expression != null) {
    		changed = this.translateNamedId(expression, this.apsFormIndex, "outcomeForm") || changed;
    		changed = this.translateNamedId(expression, this.apsFormIndex, "rightOutcomeForm") || changed;
    		
    		expression = (ObjectNode)expression.get("nextCondition");
		}
		
    	return changed;
	}
	
	
	
	private boolean translateSubprocesses(ObjectNode jsonDescriptor, String processName) {
		this.logger.trace("Translating subprocesses in process: {}", processName);
		boolean changed = false;
		
		for (JsonNode jsonChildShape : this.getChildShapes(jsonDescriptor)) {
			try {
				JsonNode jsonSubprocessRef = (JsonNode)jsonChildShape.get("properties").get("subprocessreference");
				changed = this.translateReference(jsonSubprocessRef, this.apsIndex) || changed;
			} catch (NullPointerException npe) {
				// suppress; gracefully skip
			}
		}
		
		return changed;
	}
	

	
	private boolean translateReference(JsonNode _jsonRef, Index<Long, String> apsIndex) {
		return this.translateNamedId(_jsonRef, apsIndex, null);
	}
	
	private boolean translateNamedId(JsonNode _jsonRef, Index<Long, String> apsIndex, String prefix) {
		if (_jsonRef == null || !_jsonRef.isObject())
			return false;
		
		String nameField = prefix == null ? "name" : prefix + "Name";
		String idField = prefix == null ? "id" : prefix + "Id";
		
		ObjectNode jsonRef = (ObjectNode)_jsonRef;
		JsonNode jsonName = jsonRef.get(nameField);
		if (jsonName == null) {
            this.logger.trace("The expression does not have an APS model name field ({})", nameField);
            return false;
		}
		
		String modelName = jsonName.asText();
		this.logger.trace("Found model '{}' in the APS Process descriptor", modelName);
		if (!apsIndex.containsValue(modelName)) {
            this.logger.debug("The model '{}' does not exists in APS; leaving unchanged", modelName);
            return false;
		}
		
		JsonNode jsonId = jsonRef.get(idField);
		long modelId = jsonId == null ? -1L : jsonId.asLong();
		long apsModelId = apsIndex.getFirstKey(modelName);
		if (apsModelId != modelId) {
			this.logger.debug("The model '{}' exists in APS with ID {}", modelName, apsModelId);
			jsonRef.put(idField, apsModelId);
			return true;
		} else {
			this.logger.trace("The model '{}' ID does not change; leaving unchanged", modelName);
	        return false;
		}
	}
	
	
	
	private ArrayNode getChildShapes(ObjectNode jsonDescriptor) {
		JsonNode jsonChildShapes = jsonDescriptor.get("childShapes");
		if (jsonChildShapes == null)
			jsonChildShapes = jsonDescriptor.get("editorJson").get("childShapes");
		return (ArrayNode)jsonChildShapes;
	}

}

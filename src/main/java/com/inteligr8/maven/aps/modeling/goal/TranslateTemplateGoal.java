/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.maven.aps.modeling.crawler.ApsTemplateCrawler;
import com.inteligr8.maven.aps.modeling.translator.ApsTemplateTranslator;

/**
 * A class that implements an APS template translation goal.
 * 
 * This goal will translate all the JSON template files to match the
 * environment referenced by the specified APS service.  This relies on all APS
 * templates having unique names.  The names of those templates are used to
 * remap IDs between environments.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "translate-template", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class TranslateTemplateGoal extends ApsAddressibleGoal {
	
	@Parameter( property = "aps-model.template.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File templateDirectory;

	@Parameter( property = "aps-model.translatedTemplate.directory", required = false, defaultValue = "${project.build.directory}/aps" )
	protected File finalDirectory;

	@Parameter( property = "aps-model.translate.overwrite", required = true, defaultValue = "true" )
	protected boolean overwrite = true;
	
	@Parameter( property = "aps-model.translate.charset", required = true, defaultValue = "utf-8" )
	protected String charsetName = "utf-8";
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.validateApsTemplateDirectory();
		this.validateTargetDirectory();
		
		try {
			if (!this.templateDirectory.equals(this.finalDirectory)) {
				FileUtils.copyDirectory(this.templateDirectory, this.finalDirectory);
			}

			ApsTemplateTranslator translator = new ApsTemplateTranslator(this.getApsApi());
			translator.buildIndexes();
			
			ApsTemplateCrawler crawler = new ApsTemplateCrawler(this.finalDirectory, true);
			crawler.execute(translator);
		} catch (IOException ie) {
			throw new MojoFailureException("An I/O issue occurred", ie);
		} catch (IllegalArgumentException iae) {
			throw new MojoExecutionException("The input is not supported", iae);
		} catch (IllegalStateException ise) {
			throw new MojoExecutionException("The state of system is not supported", ise);
		}
	}
	
	protected void validateApsTemplateDirectory() throws MojoExecutionException {
		if (!this.templateDirectory.exists())
			throw new MojoExecutionException("The 'templateDirectory' does not exist: " + this.templateDirectory);
		if (!this.templateDirectory.isDirectory())
			throw new MojoExecutionException("The 'templateDirectory' is not a directory: " + this.templateDirectory);
	}
	
	protected void validateTargetDirectory() throws MojoExecutionException {
		if (!this.finalDirectory.exists()) {
			this.finalDirectory.mkdirs();
		} else if (!this.finalDirectory.isDirectory()) {
			throw new MojoExecutionException("The 'finalDirectory' is not a directory: " + this.finalDirectory);
		}
	}

}

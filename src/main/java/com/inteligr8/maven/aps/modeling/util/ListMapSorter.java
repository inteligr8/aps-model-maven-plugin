/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * A singleton class that provides list of maps sorting methods.
 * 
 * @author brian@inteligr8.com
 */
public class ListMapSorter {
	
	private static ListMapSorter INSTANCE = new ListMapSorter();
	
	/**
	 * @return A singleton instance of this class.
	 */
	public static ListMapSorter getInstance() {
		return INSTANCE;
	}



	protected ListMapSorter() {
	}
	
	/**
	 * This method sorts the maps in the list by the value of the specified key
	 * in the maps.
	 * 
	 * If a map has the specified key, its value is subject to the standard
	 * comparison.  If it doesn't have a value, then it will sort to the end of
	 * the array.
	 * 
	 * @see MapComparator
	 * 
	 * @param <V> The type of value in the maps.
	 * @param listOfMaps A list of maps.
	 * @param mapKey A key in the maps.
	 * @return true if any maps were moved/sorted; false if unchanged.
	 */
	public <V> boolean sort(List<Map<String, V>> listOfMaps, String mapKey) {
		return this.sort(listOfMaps, new MapComparator<String, V>(mapKey));
	}

	/**
	 * This method sorts the maps of the list using the specified comparator.
	 * 
	 * @param <K> The type of key in the maps.
	 * @param <V> The type of value in the maps.
	 * @param listOfMaps A list of maps.
	 * @param comparator A map comparator.
	 * @return true if any maps were moved/sorted; false if unchanged.
	 */
	public <K, V> boolean sort(List<Map<K, V>> listOfMaps, Comparator<Map<K, V>> comparator) {
		if (listOfMaps.isEmpty())
			return false;
		
		boolean sorted = false;
		boolean changed = false;
		int index = 0;
		Map<K, V> map = listOfMaps.get(index);

		// simple bubble sort
		while (!sorted) {
			sorted = true;
			int nextIndex = index + 1;
			
			// forward bubble
			while (nextIndex < listOfMaps.size()) {
				Map<K, V> nextMap = listOfMaps.get(nextIndex);
				int c = comparator.compare(map, nextMap);
				
				if (c == 0) {
					map = nextMap;
				} else if (c < 0) {
					map = nextMap;
				} else {
					listOfMaps.add(index, listOfMaps.remove(nextIndex));
					sorted = false;
					changed = true;
				}

				index = nextIndex;
				nextIndex = index + 1;
			}
			
			if (sorted)
				break;
			sorted = true;
			nextIndex = index - 1;

			// reverse bubble
			while (nextIndex >= 0) {
				Map<K, V> nextMap = listOfMaps.get(nextIndex);
				int c = comparator.compare(nextMap, map);
				
				if (c == 0) {
					map = nextMap;
				} else if (c < 0) {
					map = nextMap;
				} else {
					listOfMaps.add(nextIndex, listOfMaps.remove(index));
					sorted = false;
					changed = true;
				}

				index = nextIndex;
				nextIndex = index - 1;
			}
		}
		
		return changed;
	}

}

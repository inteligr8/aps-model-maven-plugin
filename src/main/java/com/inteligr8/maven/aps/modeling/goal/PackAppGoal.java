/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

/**
 * A class that implements an APS App packaging goal.
 * 
 * This goal will simply pack (zip) a folder containing an APS App file
 * structure.  If a file exists with a conflicting name, it will be
 * overwritten.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "pack-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class PackAppGoal extends ApsAppGoal {

	@Parameter( property = "aps-model.app.directory", required = true, defaultValue = "${project.build.directory}/aps/app" )
	protected File unzipDirectory;
	
	@Parameter( property = "aps-model.upload.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File zipDirectory;
	
	protected final int bufferSize = 128 * 1024;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.validateSourceDirectory();
		File appDirectory = this.validateAppDirectory();
		
		File targetFile = this.validateTargetDirectory();
		
		try {
			this.pack(appDirectory, targetFile);
		} catch (IOException ie) {
			throw new MojoFailureException("The APS App could not be packed", ie);
		}
	}
	
	protected void validateSourceDirectory() {
		if (!this.unzipDirectory.exists()) {
			throw new IllegalStateException("The 'unzipDirectory' does not exist: " + this.unzipDirectory);
		} else if (!this.unzipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'unzipDirectory' is not a directory: " + this.unzipDirectory);
		}
	}
	
	protected File validateAppDirectory() {
		File appDirectory = new File(this.unzipDirectory, this.apsAppName);
		if (!appDirectory.exists()) {
			throw new IllegalStateException("The 'apsAppName' does not exist in the source directory: " + this.unzipDirectory);
		} else if (!appDirectory.isDirectory()) {
			throw new IllegalStateException("The 'apsAppName' refers to a file and not a directory: " + appDirectory);
		}
		
		return appDirectory;
	}
	
	protected File validateTargetDirectory() {
		if (!this.zipDirectory.exists()) {
			this.getLog().debug("The 'zipDirectory' does not exist; creating: " + this.zipDirectory);
			this.zipDirectory.mkdirs();
		} else if (!this.zipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'zipDirectory' is not a directory: " + this.zipDirectory);
		}
		
		File targetFile = new File(this.zipDirectory, this.apsAppName + ".zip");

		if (targetFile.isDirectory()) {
			throw new IllegalStateException("The App file is not a file: " + targetFile);
		} else if (targetFile.exists()) {
			this.getLog().debug("The App file in the 'zipDirectory' exists; deleting: " + targetFile);
			targetFile.delete();
		}
		
		return targetFile;
	}
	
	private void pack(File appDirectory, File appZip) throws IOException {
		this.getLog().debug("Packing APS App to ZIP: " + appZip);
		
		FileOutputStream fostream = new FileOutputStream(appZip);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.bufferSize);
		try {
			ZipOutputStream zostream = new ZipOutputStream(bostream);
			try {
				this.zipDirectory(appDirectory, null, zostream);
				zostream.flush();
			} finally {
				zostream.close();
			}
		} finally {
			bostream.close();
		}
	}
	
	private void zipDirectory(File directory, Path path, ZipOutputStream zostream) throws IOException {
		for (File file : directory.listFiles()) {
			Path newPath = path == null ? Paths.get(file.getName()) : path.resolve(file.getName());
			
			if (file.isDirectory()) {
				this.zipDirectory(file, newPath, zostream);
			} else {
				this.zipFile(file, newPath, zostream);
			}
		}
	}
	
	private void zipFile(File file, Path path, ZipOutputStream zostream) throws IOException {
		if (file.isDirectory() || !file.exists())
			throw new IllegalStateException();

		this.getLog().debug("Packing APS file: " + path);
		
		// Activit App processing requires forward slashes (WOW) 
		ZipEntry zentry = new ZipEntry(path.toString().replace('\\', '/'));
		zostream.putNextEntry(zentry);
		try {
			FileInputStream fistream = new FileInputStream(file);
			BufferedInputStream bistream = new BufferedInputStream(fistream);
			try {
				IOUtils.copy(bistream, zostream);
			} finally {
				bistream.close();
			}
		} finally {
			zostream.closeEntry();
		}
	}

}

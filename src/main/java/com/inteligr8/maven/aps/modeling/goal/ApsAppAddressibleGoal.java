/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.ApsPublicRestApiJerseyImpl;
import com.inteligr8.alfresco.activiti.api.ModelsApi.ModelType;
import com.inteligr8.alfresco.activiti.model.ModelRepresentation;

/**
 * This class adds the APS App name to APS addressibility to extending goals.
 * 
 * Only use this class if your goal needs both the APS name and an APS service
 * client.  You can use `ApsAppGoal` or `ApsAddressibleGoal` if you only need
 * one of those capabilities.
 * 
 * @author brian@inteligr8.com
 */
public abstract class ApsAppAddressibleGoal extends ApsAddressibleGoal {

	@Parameter( property = "aps-model.appName", required = true )
	protected String apsAppName;
	
	/**
	 * This method makes the appropriate service calls to find all the APS
	 * Apps, returning them as a map of names to models.
	 * 
	 * This method does not cache the result.
	 * 
	 * @return A map of APS App names to their model; may be empty; never null.
     * @throws MojoExecutionException The APS API failed to initialize.
	 */
	protected Map<String, ModelRepresentation> buildAppNameMap() throws MojoExecutionException {
		ApsPublicRestApiJerseyImpl api = this.getApsApi();
		
		Map<String, ModelRepresentation> apps = new HashMap<>(16);

		this.getLog().debug("Searching for all APS Apps");
		ResultList<ModelRepresentation> results = api.getModelsApi().get("everyone", null, ModelType.App.getId(), null);
		this.getLog().debug("Found " + results.getTotal() + " APS Apps");
		for (ModelRepresentation model : results.getData()) {
			String name = model.getName();
			apps.put(name, model);
		}
		
		return apps;
	}
	
	/**
	 * This method makes the appropriate service calls to find the APS App by
	 * the configured APS App name.
	 * 
	 * This method does not cache the result.
	 * 
	 * @param failOnNotFound true to fail if not found; false to return null.
	 * @return An APS App model; null if not found.
	 * @throws MojoExecutionException The APS App could not be found.
	 */
	protected ModelRepresentation findAppModel(boolean failOnNotFound) throws MojoExecutionException {
		return this.findAppModelByName(this.apsAppName, failOnNotFound);
	}
	
	/**
	 * This method makes the appropriate service calls to find an APS App by
	 * the specified APS App name.
	 * 
	 * This method does not cache the result.
	 * 
	 * @param apsName An APS App name.
	 * @param failOnNotFound true to fail if not found; false to return null.
	 * @return An APS App model; null if not found.
	 * @throws MojoExecutionException The APS App could not be found.
	 */
	protected ModelRepresentation findAppModelByName(String appName, boolean failOnNotFound) throws MojoExecutionException {
		Map<String, ModelRepresentation> apps = this.buildAppNameMap();
		ModelRepresentation appModel = apps.get(this.apsAppName);
		if (failOnNotFound && appModel == null)
			throw new MojoExecutionException("The APS App '" + this.apsAppName + "' could not be found; valid apps: " + apps.keySet());
		return appModel;
	}

}

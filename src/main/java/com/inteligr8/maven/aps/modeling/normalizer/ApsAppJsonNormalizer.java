/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.maven.aps.modeling.util.ArrayNodeObjectSorter;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS App JSON configuration file normalizer.
 * 
 * This will remove the 'lastUpdated' date of each defined process model.  It
 * will also order the process 'models' by their respective 'name' values.
 * 
 * @author brian@inteligr8.com
 */
public class ApsAppJsonNormalizer implements ApsFileNormalizer {

	private final Logger logger = LoggerFactory.getLogger(ApsAppJsonNormalizer.class);
	
	private final boolean enableSorting;
	
	/**
	 * This constructor initializes the default normalizer with or without
	 * sorting.
	 * 
	 * The sorting feature is available for a better "diff" experience.  If
	 * you intend to commit the APS App configurations to Git, you will want to
	 * enable sorting.
	 * 
	 * @param enableSorting true to re-order JSON objects; false to keep as-is.
	 */
	public ApsAppJsonNormalizer(boolean enableSorting) {
		this.enableSorting = enableSorting;
	}
	
	@Override
	public void normalizeFile(File file, String modelName) throws IOException {
		this.logger.debug("Normalizing App JSON file: {}", file);

		boolean changed = false;
		JsonNode descriptor = ModelUtil.getInstance().readJson(file);
		ArrayNode jsonModels = (ArrayNode)descriptor.get("definition").get("models");
		
		// remove system-level and non-functional fields from models
		for (JsonNode _jsonModel : jsonModels) {
			ObjectNode jsonModel = (ObjectNode)_jsonModel;
			changed = this.transformModel(jsonModel) || changed;
		}
		
		// sort the models for better 'diff' support
		if (this.enableSorting) {
			boolean sorted = ArrayNodeObjectSorter.getInstance().sort(jsonModels, "name");
			this.logger.trace("Sorted App models: {}: {}", modelName, sorted);
			changed = sorted || changed;
		}
		
		if (changed)
			ModelUtil.getInstance().writeJson(descriptor, file, this.enableSorting);
	}

	private boolean transformModel(ObjectNode jsonModel) {
		this.logger.trace("Removing excess App model meta-data: {}", jsonModel.get("name"));

		int fields = jsonModel.size();
		jsonModel.remove(Arrays.asList("lastUpdated"));
//		jsonModel.remove(Arrays.asList("createdBy", "createdByFullName", "lastUpdatedBy", "lastUpdatedByFullName", "lastUpdated"));
		return jsonModel.size() < fields;
	}

}

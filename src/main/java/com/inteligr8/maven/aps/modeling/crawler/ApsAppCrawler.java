/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.crawler;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class that implements a APS App export crawler.  The crawler does not
 * directly perform any transformations.  Those are handled through a callback.
 * 
 * @author brian@inteligr8.com
 */
public class ApsAppCrawler {

	private final Logger logger = LoggerFactory.getLogger(ApsAppCrawler.class);
	private final Pattern filenamePattern = Pattern.compile("([^/]+)-([0-9]+)\\.(json|bpmn20\\.xml)");
	private final String appName;
	private final File appDirectory;
	private final File appDescriptor;
	private final boolean failOnIntegrityViolation;
	
	/**
	 * @param apsAppName A name for the APS App.
	 * @param apsAppDirectory A directory to the unpacked APS App export.
	 * @param failOnIntegrityViolation true to fail on file integrity issues; false to log warnings.
	 */
	public ApsAppCrawler(String apsAppName, File apsAppDirectory, boolean failOnIntegrityViolation) {
		this.appName = apsAppName;
		this.appDirectory = apsAppDirectory;
		this.failOnIntegrityViolation = failOnIntegrityViolation;

		this.appDescriptor = this.validateDescriptor();
		if (this.logger.isDebugEnabled())
			this.logger.debug("APS App descriptor found: " + this.appDescriptor);
	}
	
	/**
	 * @return A file handle to the root APS App JSON (configuration) file.
	 */
	protected File validateDescriptor() {
		File appDescriptor = new File(this.appDirectory, this.appName + ".json");
		
		if (!appDescriptor.exists())
			throw new IllegalStateException("The APS App descriptor could not be found: " + this.appDescriptor);
		if (!appDescriptor.isFile())
			throw new IllegalStateException("The APS App descriptor is not a file: " + this.appDescriptor);
		
		return appDescriptor;
	}
	
	/**
	 * @param crawlable A crawlable implementation; the callback for potential transformations.
	 * @throws IOException A file access exception occurred.
	 */
	public void execute(ApsAppCrawlable crawlable) throws IOException {
		this.logger.info("Crawling APS App ...");
		
		Map<String, ApsFileTransformer> processTransformers = new HashMap<>();
		processTransformers.put("json", crawlable.getProcessJsonTransformer());
		processTransformers.put("bpmn20.xml", crawlable.getProcessBpmnTransformer());
		processTransformers.put("bpmn", crawlable.getProcessBpmnTransformer());

		this.transform(crawlable.getAppJsonTransformer(), this.appDescriptor, this.appName, null);
		this.crawlModels("form-models", crawlable.getFormJsonTransformer());
		this.crawlModels("bpmn-models", processTransformers);
		this.crawlModels("bpmn-subprocess-models", crawlable.getProcessJsonTransformer());
	}
	
	protected void crawlModels(String modelsDirectoryName, ApsFileTransformer transformer) throws IOException {
		this.crawlModels(modelsDirectoryName, Collections.singletonMap("json", transformer));
	}
	
	protected void crawlModels(String modelsDirectoryName, Map<String, ApsFileTransformer> transformers) throws IOException {
		File modelsDirectory = new File(this.appDirectory, modelsDirectoryName);
		if (!modelsDirectory.exists()) {
			this.logger.debug("APS model directory doesn't exist; no models to transform: {}", modelsDirectory);
			return;
		}
		
		for (File modelFile : modelsDirectory.listFiles()) {
			this.logger.trace("Transforming model: {}", modelFile);
			
			Matcher matcher = this.filenamePattern.matcher(modelFile.getName());
			if (!matcher.find()) {
				this.logger.warn("The '{}' folder has a file with an unexpected filename format; skipping: {}", modelsDirectoryName, modelFile);
				continue;
			}
			
			String ext = matcher.group(3);
			ApsFileTransformer transformer = transformers.get(ext.toLowerCase());
			if (transformer == null) {
				this.logger.warn("The '{}' folder has a file with an unexpected extension; skipping: {}", modelsDirectoryName, modelFile);
				continue;
			}
			
			String modelName = matcher.group(1);
			Long modelId = Long.valueOf(matcher.group(2));
			this.logger.trace("Transforming model {} ID: {}", modelName, modelId);
			
			this.transform(transformer, modelFile, modelName, modelId);
		}
	}
	
	private void transform(ApsFileTransformer transformer, File modelFile, String modelName, Long modelId) throws IOException {
		try {
			transformer.transformFile(modelFile, modelName, modelId);
		} catch (IllegalArgumentException | IllegalStateException ie) {
			if (this.failOnIntegrityViolation) {
				throw ie;
			} else {
				this.logger.warn(ie.getMessage());
			}
		}
	}

}

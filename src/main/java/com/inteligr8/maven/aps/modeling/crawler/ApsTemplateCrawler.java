/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.crawler;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class that implements a APS App export crawler.  The crawler does not
 * directly perform any transformations.  Those are handled through a callback.
 * 
 * @author brian@inteligr8.com
 */
public class ApsTemplateCrawler {

	private final Logger logger = LoggerFactory.getLogger(ApsTemplateCrawler.class);
	private final File templateDirectory;
	private final boolean failOnIntegrityViolation;
	
	/**
	 * @param apsTemplateDirectory A directory to the APS template files.
	 * @param failOnIntegrityViolation true to fail on file integrity issues; false to log warnings.
	 */
	public ApsTemplateCrawler(File apsTemplateDirectory, boolean failOnIntegrityViolation) {
		this.templateDirectory = apsTemplateDirectory;
		this.failOnIntegrityViolation = failOnIntegrityViolation;
	}
	
	/**
	 * @param crawlable A crawlable implementation; the callback for potential transformations.
	 * @throws IOException A file access exception occurred.
	 */
	public void execute(ApsTemplateCrawlable crawlable) throws IOException {
		this.logger.info("Crawling APS templates ...");
		
		this.crawlTemplates(crawlable.getTemplateJsonTransformer());
	}
	
	protected void crawlTemplates(ApsFileTransformer transformer) throws IOException {
		for (File templateFile : this.templateDirectory.listFiles()) {
		    if (!templateFile.getName().endsWith(".json"))
		        continue;
		    
			this.logger.trace("Transforming template: {}", templateFile);
			
			try {
			    transformer.transformFile(templateFile, null, null);
	        } catch (IllegalArgumentException | IllegalStateException ie) {
	            if (this.failOnIntegrityViolation) {
	                throw ie;
	            } else {
	                this.logger.warn(ie.getMessage());
	            }
	        }
		}
	}

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.BaseTemplateLight;
import com.inteligr8.alfresco.activiti.model.DocumentTemplateLight;
import com.inteligr8.alfresco.activiti.model.EmailTemplateLight;

/**
 * This class adds the APS App name to APS addressibility to extending goals.
 * 
 * Only use this class if your goal needs both the APS name and an APS service
 * client.  You can use `ApsAppGoal` or `ApsAddressibleGoal` if you only need
 * one of those capabilities.
 * 
 * @author brian@inteligr8.com
 */
public abstract class ApsTemplateAddressibleGoal extends ApsAddressibleGoal {   
    
    public enum TemplateType {
        SystemEmail,
        CustomEmail,
        Document
    }

    @Parameter( property = "aps-model.documentTemplateNames", defaultValue = ".*" )
    protected String apsDocumentTemplateNames;
    @Parameter( property = "aps-model.systemEmailTemplateNames", defaultValue = ".*" )
    protected String apsSystemEmailTemplateNames;
    @Parameter( property = "aps-model.customEmailTemplateNames", defaultValue = ".*" )
    protected String apsCustomEmailTemplateNames;

    protected Map<TemplateType, Map<String, ? extends BaseTemplateLight>> findTemplates() throws MojoExecutionException {
        Long tenantId = this.findTenantId();
        return this.findTemplates(tenantId);
    }
    
    protected Map<TemplateType, Map<String, ? extends BaseTemplateLight>> findTemplates(Long tenantId) throws MojoExecutionException {
        Map<TemplateType, Map<String, ? extends BaseTemplateLight>> templates = new HashMap<>(32);
        
        Map<String, ? extends BaseTemplateLight> systemEmailTemplateNames = this.findSystemEmailTemplates(tenantId);
        if (systemEmailTemplateNames != null && !systemEmailTemplateNames.isEmpty())
            templates.put(TemplateType.SystemEmail, systemEmailTemplateNames);

        Map<String, ? extends BaseTemplateLight> customEmailTemplateNames = this.findCustomEmailTemplates(tenantId);
        if (customEmailTemplateNames != null && !customEmailTemplateNames.isEmpty())
            templates.put(TemplateType.CustomEmail, customEmailTemplateNames);

        Map<String, ? extends BaseTemplateLight> docTemplateNames = this.findDocumentTemplates(tenantId);
        if (docTemplateNames != null && !docTemplateNames.isEmpty())
            templates.put(TemplateType.Document, docTemplateNames);
        
        return templates;
    }
    
    protected Map<String, ? extends BaseTemplateLight> findSystemEmailTemplates(Long tenantId) throws MojoExecutionException {
        this.getLog().debug("Searching for all APS System Email Templates");
        
        List<Pattern> matchingPatterns = this.buildPatterns(this.apsSystemEmailTemplateNames);
        Map<String, EmailTemplateLight> map = new HashMap<>();
        
        ResultList<EmailTemplateLight> templates = this.getApsApi().getTemplatesApi().getSystemEmailTemplates(tenantId);
        for (EmailTemplateLight template : templates.getData()) {
            for (Pattern pattern : matchingPatterns) {
                if (pattern.matcher(template.getName()).matches())
                    map.put(template.getName(), template);
            }
        }

        this.getLog().debug("Found APS System Email Templates: " + map.size());
        
        return map;
    }
    
    protected Map<String, ? extends BaseTemplateLight> findCustomEmailTemplates(Long tenantId) throws MojoExecutionException {
        this.getLog().debug("Searching for all APS Custom Email Templates");
        
        List<Pattern> matchingPatterns = this.buildPatterns(this.apsCustomEmailTemplateNames);
        Map<String, EmailTemplateLight> map = new HashMap<>();
        int pageSize = 50;
        int page = 1;
        
        ResultList<EmailTemplateLight> templates = this.getApsApi().getTemplatesApi().getCustomEmailTemplates(null, (page-1) * pageSize, pageSize, "sort_by_name_asc", tenantId);
        while (!templates.getData().isEmpty()) {
            for (EmailTemplateLight template : templates.getData()) {
                for (Pattern pattern : matchingPatterns) {
                    if (pattern.matcher(template.getName()).matches())
                        map.put(template.getName(), template);
                }
            }
            
            page++;
            templates = this.getApsApi().getTemplatesApi().getCustomEmailTemplates(null, (page-1) * pageSize, pageSize, "sort_by_name_asc", tenantId);
        }

        this.getLog().debug("Found APS Custom Email Templates: " + map.size());
        
        return map;
    }
    
    protected Map<String, ? extends BaseTemplateLight> findDocumentTemplates(Long tenantId) throws MojoExecutionException {
        List<Pattern> matchingPatterns = this.buildPatterns(this.apsDocumentTemplateNames);
        Map<String, DocumentTemplateLight> map = new HashMap<>();
        int pageSize = 50;
        int page = 1;
        
        ResultList<DocumentTemplateLight> templates = this.getApsApi().getTemplatesApi().getDocumentTemplates(null, (page-1) * pageSize, pageSize, "sort_by_name_asc", tenantId);
        while (!templates.getData().isEmpty()) {
            for (DocumentTemplateLight template : templates.getData()) {
                for (Pattern pattern : matchingPatterns) {
                    if (pattern.matcher(template.getName()).matches())
                        map.put(template.getName(), template);
                }
            }
            
            page++;
            templates = this.getApsApi().getTemplatesApi().getDocumentTemplates(null, (page-1) * pageSize, pageSize, "sort_by_name_asc", tenantId);
        }

        this.getLog().debug("Found APS Document Templates: " + map.size());
        
        return map;
    }
    
    private List<Pattern> buildPatterns(String regexes) {
        if (regexes == null)
            return Collections.emptyList();
        
        List<Pattern> patterns = new LinkedList<>();
        
        for (String regex : regexes.split(",")) {
            regex = regex.trim();
            if (regex.length() > 0)
                patterns.add(Pattern.compile(regex));
        }
        
        return patterns;
    }

}

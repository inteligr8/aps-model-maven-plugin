/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS template JSON configuration file normalizer.
 * 
 * This will remove the 'createdBy' of each defined template.
 * 
 * @author brian@inteligr8.com
 */
public class ApsTemplateJsonNormalizer implements ApsFileNormalizer {

	private final Logger logger = LoggerFactory.getLogger(ApsTemplateJsonNormalizer.class);
	
	private final boolean enableSorting;
	
	/**
	 * This constructor initializes the default normalizer with or without
	 * sorting.
	 * 
	 * The sorting feature is available for a better "diff" experience.  If
	 * you intend to commit the APS App configurations to Git, you will want to
	 * enable sorting.
	 * 
	 * @param enableSorting true to re-order JSON objects; false to keep as-is.
	 */
	public ApsTemplateJsonNormalizer(boolean enableSorting) {
		this.enableSorting = enableSorting;
	}
	
	@Override
	public void normalizeFile(File file, String _unused) throws IOException {
		this.logger.debug("Normalizing template JSON file: {}", file);

		ObjectNode templateJson = (ObjectNode) ModelUtil.getInstance().readJson(file);
		boolean changed = this.transformModel(templateJson);
		
		if (changed)
			ModelUtil.getInstance().writeJson(templateJson, file, this.enableSorting);
	}

	private boolean transformModel(ObjectNode jsonModel) {
		this.logger.trace("Removing excess template meta-data: {}", jsonModel.get("name"));

		int fields = jsonModel.size();
		jsonModel.remove(Arrays.asList("createdBy"));
		return jsonModel.size() < fields;
	}

}

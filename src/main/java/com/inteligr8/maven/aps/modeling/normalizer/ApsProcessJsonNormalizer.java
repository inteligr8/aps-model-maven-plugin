/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.maven.aps.modeling.util.ArrayNodeObjectSorter;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS Process JSON configuration file normalizer.
 * 
 * This will do nothing unless sorting is enabled.  With sorting, it will order
 * the 'childShapes' by their respective 'resourceId' values.
 * 
 * @author brian@inteligr8.com
 */
public class ApsProcessJsonNormalizer implements ApsFileNormalizer {

	private final Logger logger = LoggerFactory.getLogger(ApsProcessJsonNormalizer.class);
	
	private final boolean enableSorting;
	
	/**
	 * This constructor initializes the default normalizer with or without
	 * sorting.
	 * 
	 * The sorting feature is available for a better "diff" experience.  If
	 * you intend to commit the APS App configurations to Git, you will want to
	 * enable sorting.
	 * 
	 * @param enableSorting true to re-order JSON objects; false to keep as-is.
	 */
	public ApsProcessJsonNormalizer(boolean enableSorting) {
		this.enableSorting = enableSorting;
	}
	
	@Override
	public void normalizeFile(File file, String modelName) throws IOException {
		this.logger.debug("Normalizing Process JSON file: {}", file);
		
		if (!this.enableSorting)
			return;
		
		boolean changed = false;
		
		ObjectNode jsonDescriptor = ModelUtil.getInstance().readJson(file, ObjectNode.class);
		ArrayNode jsonChildShapes = this.getChildShapes(jsonDescriptor);
		this.logger.trace("Found Process shapes: {}: {}", modelName, jsonChildShapes.size());
		
		changed = ArrayNodeObjectSorter.getInstance().sort(jsonChildShapes, "resourceId") || changed;
		this.logger.trace("Sorted Process shapes: {}: {}", modelName, changed);
		
		if (changed)
			ModelUtil.getInstance().writeJson(jsonDescriptor, file, this.enableSorting);
	}
	
	private ArrayNode getChildShapes(ObjectNode jsonDescriptor) {
		JsonNode jsonChildShapes = jsonDescriptor.get("childShapes");
		if (jsonChildShapes == null)
			jsonChildShapes = jsonDescriptor.get("editorJson").get("childShapes");
		return (ArrayNode)jsonChildShapes;
	}

}

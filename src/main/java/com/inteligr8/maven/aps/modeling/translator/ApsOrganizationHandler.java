/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.model.Group;
import com.inteligr8.alfresco.activiti.model.GroupLight;

/**
 * This class provides APS Organization support.
 * 
 * @author brian@inteligr8.com.
 */
public class ApsOrganizationHandler {

	private final Logger logger = LoggerFactory.getLogger(ApsOrganizationHandler.class);
	private final ApsPublicRestApi api;
	private final Map<String, GroupLight> apsOrgIndex;
	
	/**
	 * @param api An APS API reference.
	 * @param apsOrgIndex A map of organization IDs to organization meta-data defined in the APS service.
	 */
	public ApsOrganizationHandler(
			ApsPublicRestApi api,
			Map<String, GroupLight> apsOrgIndex) {
		this.api = api;
		this.apsOrgIndex = apsOrgIndex;
	}
	
	protected long createOrganization(String groupName) {
		this.logger.info("Creating organization '{}' in APS", groupName);
		
		GroupLight group = this.api.getAdminApi().createGroup(new Group()
				.withName(groupName)
				.withType(1L));			// an organization, not capability
		this.apsOrgIndex.put(groupName, group);
		return group.getId();
	}

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.model.BaseTemplateLight;
import com.inteligr8.alfresco.activiti.model.Tenant;
import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;
import com.inteligr8.maven.aps.modeling.crawler.ApsTemplateCrawlable;
import com.inteligr8.maven.aps.modeling.util.Index;

/**
 * This class defines an APS App package translator.
 * 
 * An APS App package is a ZIP file that contains multiple files in a
 * predictable folder hierachy with predictable file names.  It is effectively
 * an APS App interchange format specification.
 * 
 * A package must have at least a single configuration file at the root of the
 * ZIP package in the JSON format.  It must be named the APS App name.  That
 * file will then reference all the model elements contained in the ZIP.  Any
 * model elements not referenced will simply be ignored by APS and by this
 * plugin.
 * 
 * This class has methods that provide translator for all the various model
 * elements in an APS App package.
 * 
 * @author brian@inteligr8.com
 */
public class ApsTemplateTranslator implements ApsTemplateCrawlable {

	private final Logger logger = LoggerFactory.getLogger(ApsTemplateTranslator.class);
	private final ApsPublicRestApi api;
	
	private boolean indexesBuilt = false;
	private Index<Long, String> apsDocumentTemplateIndex;
	private Index<Long, String> apsCustomEmailTemplateIndex;
	
	public ApsTemplateTranslator(ApsPublicRestApi api) {
		this.api = api;
	}
	
	/**
	 * This method initializes the data required from the APS Service for the
	 * function of this class.
	 *  
	 * @throws IOException A network I/O related issue occurred.
	 */
	public synchronized void buildIndexes() throws IOException {
		if (this.indexesBuilt)
			return;
		
		this.logger.info("Building indexes ...");
		
		long tenantId = this.findTenantId();
		this.logger.debug("APS tenant ID: {}", tenantId);
		
		this.apsDocumentTemplateIndex = this.buildApsDocumentTemplateIndex(tenantId);
		this.logLarge("APS document templates: {}", this.apsDocumentTemplateIndex);
        
        this.apsCustomEmailTemplateIndex = this.buildApsCustomEmailTemplateIndex(tenantId);
        this.logLarge("APS custom email templates: {}", this.apsCustomEmailTemplateIndex);
		
		this.indexesBuilt = true;
	}
	
	private <K, V> void logLarge(String message, Index<K, V> index) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(message, index);
		} else {
			this.logger.debug(message, index.valueSet());
		}
	}
	
	@Override
	public ApsFileTransformer getTemplateJsonTransformer() {
		if (!this.indexesBuilt)
			throw new IllegalStateException("The indexes are never built");
		
		return new ApsTemplateJsonTranslator(
				this.apsDocumentTemplateIndex,
				this.apsCustomEmailTemplateIndex);
	}
	
	protected Long findTenantId() {
		List<Tenant> tenants = this.api.getAdminApi().getTenants();
		return (tenants == null || tenants.isEmpty()) ? null : tenants.iterator().next().getId();
	}
	
    protected Index<Long, String> buildApsDocumentTemplateIndex(Long tenantId) {
	    int perPage = 50;
	    int page = 1;
	    
		ResultList<? extends BaseTemplateLight> templates = this.api.getTemplatesApi().getDocumentTemplates(null, (page-1)*perPage, perPage, "sort_by_name_asc", tenantId);
        Index<Long, String> index = new Index<>(templates.getTotal() / 2, false);
        
		while (!templates.getData().isEmpty()) {
	        this.logger.debug("APS document templates found: {}-{} out of {}", templates.getStart(), (templates.getStart() + templates.getSize()), templates.getTotal());
	        
	        for (BaseTemplateLight template : templates.getData())
	            index.put(template.getId(), template.getName());
		    
		    page++;
		    templates = this.api.getTemplatesApi().getDocumentTemplates(null, (page-1)*perPage, perPage, "sort_by_name_asc", tenantId);
		}
		
		return index;
	}
    
    protected Index<Long, String> buildApsCustomEmailTemplateIndex(Long tenantId) {
        int perPage = 50;
        int page = 1;
        
        ResultList<? extends BaseTemplateLight> templates = this.api.getTemplatesApi().getCustomEmailTemplates(null, (page-1)*perPage, perPage, "sort_by_name_asc", tenantId);
        Index<Long, String> index = new Index<>(templates.getTotal() / 2, false);
        
        while (!templates.getData().isEmpty()) {
            this.logger.debug("APS document templates found: {}-{} out of {}", templates.getStart(), (templates.getStart() + templates.getSize()), templates.getTotal());
            
            for (BaseTemplateLight template : templates.getData())
                index.put(template.getId(), template.getName());
            
            page++;
            templates = this.api.getTemplatesApi().getCustomEmailTemplates(null, (page-1)*perPage, perPage, "sort_by_name_asc", tenantId);
        }
        
        return index;
    }

}

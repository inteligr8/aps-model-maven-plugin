/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.inteligr8.maven.aps.modeling.crawler.ApsAppCrawler;
import com.inteligr8.maven.aps.modeling.normalizer.ApsAppNormalizer;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * A class that implements an APS App unpackaging goal.
 * 
 * This goal will simply unpack (unzip) an APS App export into a folder.  If a
 * folder of the APS App name already exists, it will be cleared before the
 * export is unpacked into it.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "unpack-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class UnpackAppGoal extends ApsAppGoal {

	@Parameter( property = "aps-model.download.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File zipDirectory;

	@Parameter( property = "aps-model.app.directory", required = true, defaultValue = "${project.build.directory}/aps/app" )
	protected File unzipDirectory;
	
	@Parameter( property = "aps-model.reformat", required = true, defaultValue = "true" )
	protected boolean reformat = true;
	
	@Parameter( property = "aps-model.normalize" )
	protected boolean normalize;
	
	@Parameter( property = "aps-model.normalize.diffFriendly" )
	protected boolean diffFriendly;
	
	@Parameter( property = "aps-model.charset", required = true, defaultValue = "utf-8" )
	protected String charsetName = "utf-8";
	
	private final int bufferSize = 128 * 1024;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.validate();
		
		File sourceFile = this.validateSourceDirectory();

		this.validateTargetDirectory();
		File appDirectory = this.validateAppDirectory();
		this.getLog().debug("Clearing directory: " + appDirectory);
		this.clearDirectory(appDirectory);
		
		try {
			this.unpack(sourceFile, appDirectory);
		} catch (IOException ie) {
			throw new MojoFailureException("The downloaded APS App could not be unpacked", ie);
		}
		
		if (this.reformat)
			this.reformatFiles(appDirectory);
		if (this.normalize) {
			try {
				ApsAppNormalizer normalizer = new ApsAppNormalizer(this.diffFriendly);
				ApsAppCrawler crawler = new ApsAppCrawler(this.apsAppName, appDirectory, true);
				crawler.execute(normalizer);
			} catch (IOException ie) {
				throw new MojoFailureException("An I/O issue occurred", ie);
			}
		}
	}
	
	protected void validate() {
		if (this.normalize && !this.reformat) {
			this.getLog().warn("The reformat option is disabled, but normalization is enabled; enabling reformat anyway");
			this.reformat = true;
		}
	}
	
	protected File validateSourceDirectory() {
		if (!this.zipDirectory.exists()) {
			throw new IllegalStateException("The 'zipDirectory' does not exist: " + this.zipDirectory);
		} else if (!this.zipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'zipDirectory' is not a directory: " + this.zipDirectory);
		}
		
		File sourceFile = new File(this.zipDirectory, this.apsAppName + ".zip");
		
		if (!sourceFile.exists()) {
			throw new IllegalStateException("The App file does not exists in the 'zipDirectory': " + sourceFile);
		} else if (!sourceFile.isFile()) {
			throw new IllegalStateException("The App file is not a file: " + sourceFile);
		}
		
		return sourceFile;
	}
	
	protected void validateTargetDirectory() {
		if (!this.unzipDirectory.exists()) {
			this.getLog().debug("Creating unzip directory: " + this.unzipDirectory);
			this.unzipDirectory.mkdirs();
		} else if (!this.unzipDirectory.isDirectory()) {
			throw new IllegalStateException("The 'unzipDirectory' refers to a file and not a directory: " + this.unzipDirectory);
		}
	}
	
	protected File validateAppDirectory() {
		File appDirectory = new File(this.unzipDirectory, this.apsAppName);
		if (!appDirectory.exists()) {
			this.getLog().debug("Creating APS App directory: " + appDirectory);
			appDirectory.mkdirs();
		} else if (!appDirectory.isDirectory()) {
			throw new IllegalStateException("The 'apsAppsDirectory' has a file where nothing or a directory is expected: " + appDirectory);
		}
		
		return appDirectory;
	}
	
	private void unpack(File appZip, File targetDirectory) throws IOException {
		FileInputStream fistream = new FileInputStream(appZip);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
		try {
			ZipInputStream zistream = new ZipInputStream(bistream);
			try {
				for (ZipEntry zentry = zistream.getNextEntry(); zentry != null; zentry = zistream.getNextEntry()) {
					try {
						String path = zentry.getName();
						if (!zentry.isDirectory()) {
							this.getLog().debug("Unzipping path: " + zentry.getName());
							this.unzipToPath(zistream, Paths.get(path), targetDirectory);
						}
					} finally {
						zistream.closeEntry();
					}
				}
			} finally {
				zistream.close();
			}
		} finally {
			bistream.close();
		}
	}
	
	private void unzipToPath(InputStream istream, Path relativePath, File targetDirectory) throws IOException {
		Path path = targetDirectory.toPath().resolve(relativePath);
		Path folderPath = path.getParent();
		Files.createDirectories(folderPath);
		
		FileOutputStream fostream = new FileOutputStream(path.toFile());
		BufferedOutputStream bostream = new BufferedOutputStream(fostream);
		try {
			IOUtils.copy(istream, bostream);
		} finally {
			bostream.close();
		}
	}
	
	private void clearDirectory(File directory) {
		for (File file : directory.listFiles()) {
			if (file.isDirectory())
				this.clearDirectory(file);
			file.delete();
		}
	}
	
	private void reformatFiles(File directory) throws MojoFailureException {
		for (File file : directory.listFiles()) {
			if (file.isDirectory()) {
				this.reformatFiles(file);
			} else {
				String ext = this.getFileExtension(file);
				
				try {
					switch (ext) {
						case "json":
							this.getLog().debug("Reformatting file: " + file);
							Map<String, Object> json = ModelUtil.getInstance().readJsonAsMap(file);
							ModelUtil.getInstance().writeJson(json, file, false);
							break;
						case "xml":
						case "bpmn":
							this.getLog().debug("Reformatting file: " + file);
							Document xml = ModelUtil.getInstance().readXml(file);
							ModelUtil.getInstance().writeXml(xml, file);
							break;
						default:
							file.delete();
					}
				} catch (TransformerException | SAXException | IOException e) {
					throw new MojoFailureException("The following file failed to be reformatted: " + file, e);
				}
			}
		}
	}
	
	private String getFileExtension(File file) {
		int lastdot = file.getName().lastIndexOf('.');
		if (lastdot < 0)
			return null;
		
		return file.getName().substring(lastdot+1).toLowerCase();
	}

}

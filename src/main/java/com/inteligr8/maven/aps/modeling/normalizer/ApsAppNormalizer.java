/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import com.inteligr8.maven.aps.modeling.crawler.ApsAppCrawlable;
import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;

/**
 * This class defines an APS App package normalizer.
 * 
 * An APS App package is a ZIP file that contains multiple files in a
 * predictable folder hierachy with predictable file names.  It is effectively
 * an APS App interchange format specification.
 * 
 * A package must have at least a single configuration file at the root of the
 * ZIP package in the JSON format.  It must be named the APS App name.  That
 * file will then reference all the model elements contained in the ZIP.  Any
 * model elements not referenced will simply be ignored by APS and by this
 * plugin.
 * 
 * This class has methods that provide normalizer for all the various model
 * elements in an APS App package.
 * 
 * @author brian@inteligr8.com
 */
public class ApsAppNormalizer implements ApsAppCrawlable {

	private final boolean enableSorting;
	
	/**
	 * This constructor initializes the default normalizer with or without
	 * sorting.
	 * 
	 * The sorting feature is available for a better "diff" experience.  If
	 * you intend to commit the APS App configurations to Git, you will want to
	 * enable sorting.
	 * 
	 * @param enableSorting true to re-order JSON objects; false to keep as-is.
	 */
	public ApsAppNormalizer(boolean enableSorting) {
		this.enableSorting = enableSorting;
	}
	
	@Override
	public ApsFileTransformer getAppJsonTransformer() {
		return new ApsAppJsonNormalizer(this.enableSorting);
	}
	
	@Override
	public ApsFileTransformer getFormJsonTransformer() {
		return new ApsFormJsonNormalizer();
	}
	
	@Override
	public ApsFileTransformer getProcessJsonTransformer() {
		return new ApsProcessJsonNormalizer(this.enableSorting);
	}
	
	@Override
	public ApsFileTransformer getProcessBpmnTransformer() {
		return new ApsProcessBpmnNormalizer();
	}

}

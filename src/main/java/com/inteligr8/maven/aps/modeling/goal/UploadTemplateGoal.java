/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.inteligr8.alfresco.activiti.model.DocumentTemplateLight;
import com.inteligr8.alfresco.activiti.model.EmailTemplate;
import com.inteligr8.alfresco.activiti.model.FileMultipartJersey;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * A class that implements an APS service upload goal.
 * 
 * This goal will simply upload the APS templates within the specified template
 * directory to the specified APS service.  Any IDs specified in the uploaded
 * templates must match existing IDs for them to properly version.  That is the
 * main purpose of this plugin and can be achieved using the
 * 'translate-template' goal.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "upload-template", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class UploadTemplateGoal extends ApsAddressibleGoal {

	@Parameter( property = "aps-model.upload.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File templateDirectory;

	@Parameter( required = true, defaultValue = "false" )
	protected boolean alwaysOverwrite;

	@Parameter( property = "aps-model.dryRun", required = true, defaultValue = "false" )
	protected boolean dryRun;
    
    protected final int bufferSize = 128 * 1024;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
        this.validateSourceDirectory();

        Long tenantId = this.findTenantId();
        
        for (File file : this.templateDirectory.listFiles()) {
            if (!file.getName().endsWith(".json")) {
                this.getLog().debug("Ignoring file: " + file.getName());
                continue;
            }
            
            try {
                if (file.getName().endsWith(".dt.json")) {
                    DocumentTemplateLight template = ModelUtil.getInstance().writePojo(ModelUtil.getInstance().readJsonAsMap(file), DocumentTemplateLight.class);
                    if (!this.alwaysOverwrite && template.getId() != null && template.getCreated() != null) {
                        DocumentTemplateLight serverSideTemplate = this.getApsApi().getTemplatesApi().getDocumentTemplate(template.getId());
                        if (serverSideTemplate != null && !serverSideTemplate.getCreated().isBefore(template.getCreated())) {
                            this.getLog().debug("Document template unchanged; not updating: " + template.getId());
                            continue;
                        }
                    }
                    
                    File dfile = new File(file.getParent(), file.getName().substring(0, file.getName().length() - ".dt.json".length()));
                    if (!dfile.exists())
                        throw new FileNotFoundException("The file, '" + dfile.getName() + "' was expected and not found");
                    
                    FileInputStream fistream = new FileInputStream(dfile);
                    BufferedInputStream bistream = new BufferedInputStream(fistream, this.bufferSize);
                    try {
                        FileMultipartJersey multipart = FileMultipartJersey.from(dfile.getName(), bistream);
                        if (template.getId() == null) {
                            if (this.dryRun) {
                                this.getLog().info("[DRYRUN]: Creating document template: " + template.getName());
                            } else {
                                this.getLog().info("Creating document template: " + template.getName());
                                this.getApsApi().getTemplatesJerseyApi().createDocumentTemplate(tenantId, multipart);
                            }
                        } else {
                            if (this.dryRun) {
                                this.getLog().info("[DRYRUN]: Updating document template: " + template.getName());
                            } else {
                                this.getLog().info("Updating document template: " + template.getName());
                                this.getApsApi().getTemplatesJerseyApi().updateDocumentTemplate(template.getId(), tenantId, multipart);
                            }
                        }
                    } finally {
                        bistream.close();
                        fistream.close();
                    }
                } else if (file.getName().endsWith(".cet.json")) {
                    EmailTemplate template = ModelUtil.getInstance().writePojo(ModelUtil.getInstance().readJsonAsMap(file), EmailTemplate.class);
                    if (!this.alwaysOverwrite && template.getId() != null && template.getCreated() != null) {
                        EmailTemplate serverSideTemplate = this.getApsApi().getTemplatesApi().getCustomEmailTemplate(template.getId(), tenantId);
                        if (serverSideTemplate != null && !serverSideTemplate.getCreated().isBefore(template.getCreated())) {
                            this.getLog().debug("Custom email template unchanged; not updating: " + template.getId());
                            continue;
                        }
                    }

                    if (template.getId() == null) {
                        if (this.dryRun) {
                            this.getLog().info("[DRYRUN]: Creating custom email template: " + template.getName());
                        } else {
                            this.getLog().info("Creating custom email template: " + template.getName());
                            this.getApsApi().getTemplatesJerseyApi().createCustomEmailTemplate(template);
                        }
                    } else {
                        if (this.dryRun) {
                            this.getLog().info("[DRYRUN]: Updating custom email template: " + template.getName());
                        } else {
                            this.getLog().info("Updating custom email template: " + template.getName());
                            this.getApsApi().getTemplatesJerseyApi().updateCustomEmailTemplate(template.getId(), template);
                        }
                    }
                } else if (file.getName().endsWith(".set.json")) {
                    EmailTemplate template = ModelUtil.getInstance().writePojo(ModelUtil.getInstance().readJsonAsMap(file), EmailTemplate.class);
                    if (!this.alwaysOverwrite) {
                        EmailTemplate serverSideTemplate = this.getApsApi().getTemplatesApi().getSystemEmailTemplate(template.getName(), tenantId);
                        if (serverSideTemplate != null && template.getTemplate().equals(serverSideTemplate.getTemplate())) {
                            this.getLog().debug("System email template unchanged; not updating: " + template.getName());
                            continue;
                        }
                    }

                    if (this.dryRun) {
                        this.getLog().info("[DRYRUN]: Updating system email template: " + template.getName());
                    } else {
                        this.getLog().info("Updating system email template: " + template.getName());
                        this.getApsApi().getTemplatesJerseyApi().updateSystemEmailTemplate(template.getName(), template);
                    }
                }
            } catch (JsonProcessingException jpe) {
                throw new MojoFailureException("The APS templates JSON files could not be parsed", jpe);
            } catch (IOException ie) {
                throw new MojoFailureException("The APS templates could not be uploaded", ie);
            } catch (ParseException pe) {
                throw new MojoFailureException("This should never happen", pe);
            }
        }
	}
    
    protected void validateSourceDirectory() {
        if (!this.templateDirectory.exists()) {
            throw new IllegalStateException("The 'templateDirectory' does not exist: " + this.templateDirectory);
        } else if (!this.templateDirectory.isDirectory()) {
            throw new IllegalStateException("The 'templateDirectory' is not a directory: " + this.templateDirectory);
        }
    }

}

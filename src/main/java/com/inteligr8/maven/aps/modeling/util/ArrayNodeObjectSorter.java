/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.util.Comparator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * A singleton class that provides JSON array sorting methods.
 * 
 * @author brian@inteligr8.com
 */
public class ArrayNodeObjectSorter {
	
	private static ArrayNodeObjectSorter INSTANCE = new ArrayNodeObjectSorter();
	
	/**
	 * @return A singleton instance of this class.
	 */
	public static ArrayNodeObjectSorter getInstance() {
		return INSTANCE;
	}
	
	
	
	protected ArrayNodeObjectSorter() {
	}

	/**
	 * This method sorts the JSON object elements of the JSON array by the
	 * value of the specified field in the JSON objects.
	 * 
	 * If a JSON object has the specified field, its value is subject to the
	 * standard comparison.  If it doesn't have a value, then it will sort to
	 * the end of the array.
	 * 
	 * @see ObjectNodeComparator
	 * 
	 * @param arrayNode A JSON array of JSON objects.
	 * @param jsonObjectFieldName A field in the JSON object elements.
	 * @return true if any elements were moved/sorted; false if unchanged.
	 */
	public boolean sort(ArrayNode arrayNode, String jsonObjectFieldName) {
		return this.sort(arrayNode, new ObjectNodeComparator(jsonObjectFieldName));
	}
	
	/**
	 * This method sorts the JSON node elements of the JSON array using the
	 * specified comparator.
	 * 
	 * @param arrayNode A JSON array of JSON objects.
	 * @param comparator A JSON node comparator.
	 * @return true if any elements were moved/sorted; false if unchanged.
	 */
	public boolean sort(ArrayNode arrayNode, Comparator<JsonNode> comparator) {
		if (arrayNode.isEmpty())
			return false;
		
		boolean sorted = false;
		boolean changed = false;
		int index = 0;
		JsonNode node = arrayNode.get(index);

		// simple bubble sort
		while (!sorted) {
			sorted = true;
			int nextIndex = index + 1;
			
			// forward bubble
			while (nextIndex < arrayNode.size()) {
				JsonNode nextNode = arrayNode.get(nextIndex);
				int c = comparator.compare(node, nextNode);
				
				if (c == 0) {
					node = nextNode;
				} else if (c < 0) {
					node = nextNode;
				} else {
					arrayNode.insert(index, arrayNode.remove(nextIndex));
					sorted = false;
					changed = true;
				}

				index = nextIndex;
				nextIndex = index + 1;
			}
			
			if (sorted)
				break;
			sorted = true;
			nextIndex = index - 1;

			// reverse bubble
			while (nextIndex >= 0) {
				JsonNode nextNode = arrayNode.get(nextIndex);
				int c = comparator.compare(nextNode, node);
				
				if (c == 0) {
					node = nextNode;
				} else if (c < 0) {
					node = nextNode;
				} else {
					arrayNode.insert(nextIndex, arrayNode.remove(index));
					sorted = false;
					changed = true;
				}

				index = nextIndex;
				nextIndex = index - 1;
			}
		}
		
		return changed;
	}

}

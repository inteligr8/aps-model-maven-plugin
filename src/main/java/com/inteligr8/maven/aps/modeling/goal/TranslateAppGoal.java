/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.maven.aps.modeling.crawler.ApsAppCrawler;
import com.inteligr8.maven.aps.modeling.translator.ApsAppTranslator;

/**
 * A class that implements an APS App translation goal.
 * 
 * This goal will translate all the JSON and XML files in an APS App to match
 * the environment referenced by the specified APS App.  This relies on all APS
 * model elements (apps, processes, and forms) to have unique names.  The names
 * of those model elements are used to remap IDs between environments.
 * 
 * APS does not enforce a unique name constraint.  But it is good practice to
 * avoid using the same name anyhow.  This plugin will just make you do it.  It
 * will error if a duplicate is detected.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "translate-app", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class TranslateAppGoal extends ApsAppAddressibleGoal {
	
	@Parameter( property = "aps-model.app.directory", required = true, defaultValue = "${project.build.directory}/aps/app" )
	protected File unzipDirectory;

	@Parameter( property = "aps-model.translatedApp.directory", required = false, defaultValue = "${project.build.directory}/aps/app" )
	protected File finalDirectory;

	@Parameter( property = "aps-model.translate.overwrite", required = true, defaultValue = "true" )
	protected boolean overwrite = true;
	
	@Parameter( property = "aps-model.translate.charset", required = true, defaultValue = "utf-8" )
	protected String charsetName = "utf-8";
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		File apsAppDirectory = this.validateApsAppDirectory();
		File newApsAppDirectory = this.validateTargetDirectory();
		
		try {
			if (!apsAppDirectory.equals(newApsAppDirectory)) {
				FileUtils.copyDirectory(apsAppDirectory, newApsAppDirectory);
				apsAppDirectory = newApsAppDirectory;
			}

			ApsAppTranslator translator = new ApsAppTranslator(apsAppDirectory, this.getApsApi());
			translator.buildIndexes();
			
			ApsAppCrawler crawler = new ApsAppCrawler(this.apsAppName, apsAppDirectory, true);
			crawler.execute(translator);
		} catch (IOException ie) {
			throw new MojoFailureException("An I/O issue occurred", ie);
		} catch (IllegalArgumentException iae) {
			throw new MojoExecutionException("The input is not supported", iae);
		} catch (IllegalStateException ise) {
			throw new MojoExecutionException("The state of system is not supported", ise);
		}
	}
	
	protected File validateApsAppDirectory() throws MojoExecutionException {
		if (!this.unzipDirectory.exists())
			throw new MojoExecutionException("The 'unzipDirectory' does not exist: " + this.unzipDirectory);
		if (!this.unzipDirectory.isDirectory())
			throw new MojoExecutionException("The 'unzipDirectory' is not a directory: " + this.unzipDirectory);
		
		File appDirectory = new File(this.unzipDirectory, this.apsAppName);

		if (!appDirectory.exists())
			throw new MojoExecutionException("The 'apsAppName' does not exist in the 'unzipDirectory': " + appDirectory);
		if (!appDirectory.isDirectory())
			throw new MojoExecutionException("The 'apsAppName' is not a directory in the 'unzipDirectory': " + appDirectory);
		
		return appDirectory;
	}
	
	protected File validateTargetDirectory() throws MojoExecutionException {
		if (!this.finalDirectory.exists()) {
			this.finalDirectory.mkdirs();
		} else if (!this.finalDirectory.isDirectory()) {
			throw new MojoExecutionException("The 'finalDirectory' is not a directory: " + this.finalDirectory);
		}
		
		File appDirectory = new File(this.finalDirectory, this.apsAppName);

		if (!appDirectory.exists()) {
			appDirectory.mkdirs();
		} else if (!appDirectory.isDirectory()) {
			throw new MojoExecutionException("The 'apsAppName' is not a directory in the 'finalDirectory': " + appDirectory);
		}
		
		return appDirectory;
	}

}

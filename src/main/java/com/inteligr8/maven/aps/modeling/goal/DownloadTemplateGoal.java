/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inteligr8.alfresco.activiti.model.BaseTemplateLight;
import com.inteligr8.alfresco.activiti.model.EmailTemplate;
import com.inteligr8.maven.aps.modeling.normalizer.ApsTemplateJsonNormalizer;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * A class that implements an APS service download goal.
 * 
 * This goal will simply download the APS templates with the specified names
 * from the specified APS service.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "download-template", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class DownloadTemplateGoal extends ApsTemplateAddressibleGoal {

	@Parameter( property = "aps-model.download.directory", required = true, defaultValue = "${project.build.directory}/aps" )
	protected File templateDirectory;
    
    @Parameter( property = "aps-model.normalize" )
    protected boolean normalize;
    
    @Parameter( property = "aps-model.normalize.diffFriendly" )
    protected boolean diffFriendly;
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.validateTargetDirectory();

        Long tenantId = this.findTenantId();
		
		try {
	        Map<TemplateType, Map<String, ? extends BaseTemplateLight>> templates = this.findTemplates(tenantId);
	        for (TemplateType ttype : templates.keySet()) {
	            this.getLog().info("Downloading " + templates.get(ttype).size() + " " + ttype + " templates");
	            
	            for (Entry<String, ? extends BaseTemplateLight> template : templates.get(ttype).entrySet()) {
	                switch (ttype) {
	                    case Document:
	                        File dfilebin = new File(this.templateDirectory, template.getValue().getName());
	                        
	                        Response response = this.getApsApi().getTemplatesApi().getDocumentTemplate(
									template.getValue().getId(),
									System.currentTimeMillis());
							try {
								if (response.getStatus() / 100 == 2) {
									InputStream istream = (InputStream) response.getEntity();
									try {
										FileUtils.copyInputStreamToFile(istream, dfilebin);
									} finally {
										istream.close();
									}
								} else {
									this.getLog().warn("The document template could not be downloaded; skipping: " + template.getValue().getName());
									continue;
								}
							} finally {
								response.close();
							}

	                        ObjectNode djson = ModelUtil.getInstance().readPojo(template.getValue());
	                        File dfile = new File(this.templateDirectory, template.getValue().getName() + ".dt.json");
	                        ModelUtil.getInstance().writeJson(djson, dfile, this.diffFriendly);
	                        if (this.normalize)
	                            new ApsTemplateJsonNormalizer(this.diffFriendly).normalizeFile(dfile, null);
							
	                        break;
	                    case CustomEmail:
	                        EmailTemplate etemplate = this.getApsApi().getTemplatesApi().getCustomEmailTemplate(template.getValue().getId(), tenantId);
	                        ObjectNode ejson = ModelUtil.getInstance().readPojo(etemplate);
	                        File efile = new File(this.templateDirectory, template.getValue().getName() + ".cet.json");
	                        ModelUtil.getInstance().writeJson(ejson, efile, this.diffFriendly);
                            if (this.normalize)
                                new ApsTemplateJsonNormalizer(this.diffFriendly).normalizeFile(efile, null);
                            
	                        break;
	                    case SystemEmail:
	                        EmailTemplate stemplate = this.getApsApi().getTemplatesApi().getSystemEmailTemplate(template.getValue().getName(), tenantId);
	                        ObjectNode sjson =  ModelUtil.getInstance().readPojo(stemplate);
	                        File sfile = new File(this.templateDirectory, template.getValue().getName() + ".set.json");
	                        ModelUtil.getInstance().writeJson(sjson, sfile, this.diffFriendly);
                            if (this.normalize)
                                new ApsTemplateJsonNormalizer(this.diffFriendly).normalizeFile(sfile, null);
	                }
	            }
	        }
		} catch (IOException ie) {
			throw new MojoFailureException("The downloaded APS templates could not be saved", ie);
		}
	}
	
	protected void validateTargetDirectory() {
		if (!this.templateDirectory.exists()) {
			this.getLog().debug("Creating APS template directory: " + this.templateDirectory);
			this.templateDirectory.mkdirs();
		} else if (!this.templateDirectory.isDirectory()) {
			throw new IllegalStateException("The 'templateDirectory' refers to a file and not a directory");
		}
	}

}

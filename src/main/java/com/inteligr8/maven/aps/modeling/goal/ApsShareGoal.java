/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.api.ModelsApi;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.alfresco.activiti.model.ModelRepresentation;
import com.inteligr8.alfresco.activiti.model.PermissionLevel;
import com.inteligr8.alfresco.activiti.model.PermissionLight;
import com.inteligr8.alfresco.activiti.model.ShareInfoRequest;
import com.inteligr8.alfresco.activiti.model.SharePermission;
import com.inteligr8.alfresco.activiti.model.Tenant;
import com.inteligr8.maven.aps.modeling.util.Index;

/**
 * A class that implements a way to 'share' APS models.
 * 
 * All APS models are automatically owned by the person that creates them.  The
 * model is then not viewable or editable by anyone else, except for APS
 * administrators.  This class provides a way to automatically 'share' the
 * synchronized models to groups or individual users.  The 'share' may have
 * readonly or editing capabilities, as configured.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "share-models", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class ApsShareGoal extends ApsAddressibleGoal {

	@Parameter( property = "aps-model.share.modelName" )
	protected String modelName;

	@Parameter( property = "aps-model.share.readers" )
	protected String readers;

	@Parameter( property = "aps-model.share.editors" )
	protected String editors;

	@Parameter( property = "aps-model.share.app.readers" )
	protected String appReaders;

	@Parameter( property = "aps-model.share.app.editors" )
	protected String appEditors;

	@Parameter( property = "aps-model.share.process.readers" )
	protected String processReaders;

	@Parameter( property = "aps-model.share.process.editors" )
	protected String processEditors;

	@Parameter( property = "aps-model.share.form.readers" )
	protected String formReaders;

	@Parameter( property = "aps-model.share.form.editors" )
	protected String formEditors;

	@Parameter( property = "aps-model.share.doRevoke", defaultValue = "false" )
	protected boolean doRevoke = false;

	protected Set<String> appReaderSet;
	protected Set<String> appEditorSet;
	protected Set<String> processReaderSet;
	protected Set<String> processEditorSet;
	protected Set<String> formReaderSet;
	protected Set<String> formEditorSet;
	
	private Index<String, Long> identityIndex = new Index<>(128, true);
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		this.normalizeParameters();
		this.buildIdentityIndex();
		
		if (!this.appReaderSet.isEmpty() || !this.appEditorSet.isEmpty())
			this.shareModels(ModelsApi.ModelType.App, this.appReaderSet, this.appEditorSet);

		if (!this.processReaderSet.isEmpty() || !this.processEditorSet.isEmpty())
			this.shareModels(ModelsApi.ModelType.Process, this.processReaderSet, this.processEditorSet);

		if (!this.formReaderSet.isEmpty() || !this.formEditorSet.isEmpty())
			this.shareModels(ModelsApi.ModelType.Form, this.formReaderSet, this.formEditorSet); 
	}
	
	private void shareModels(ModelsApi.ModelType modelType, Set<String> readers, Set<String> editors) throws MojoExecutionException {
		ResultList<ModelRepresentation> models = this.getApsApi().getModelsApi().get(null, null, modelType.getId(), null);
		if (models.getData() == null)
			return;
		
		for (ModelRepresentation model : models.getData()) {
			if (this.modelName != null && !this.modelName.equals(model.getName()))
				continue;
			
			Set<String> groupsAddressed = new HashSet<>();
			Set<String> readersUnaddressed = new HashSet<>(readers);
			Set<String> editorsUnaddressed = new HashSet<>(editors);
			ShareInfoRequest changeRequest = new ShareInfoRequest();
			
			ResultList<SharePermission> shares = this.getApsApi().getShareApi().getShareInfo(model.getId().toString());
			if (shares.getData() != null) {
				for (SharePermission share : shares.getData()) {
					if (share.getGroup() != null) {
						groupsAddressed.add(share.getGroup().getName());
						this.analyzeChanges(modelName, readers, editors, share, changeRequest);
					} else if (share.getPerson() != null) {
						this.getLog().debug("Person-based model sharing not supported at this time; ignoring");
					}
				}
			}
			
			readersUnaddressed.removeAll(groupsAddressed);
			for (String reader : readersUnaddressed)
				this.analyzeChanges(modelName, reader, PermissionLevel.Read, changeRequest);

			editorsUnaddressed.removeAll(groupsAddressed);
			for (String editor : editorsUnaddressed)
				this.analyzeChanges(modelName, editor, PermissionLevel.Write, changeRequest);
			
			if (!changeRequest.getAdded().isEmpty() || !changeRequest.getUpdated().isEmpty() || !changeRequest.getRemoved().isEmpty()) {
				this.getLog().info("Sharing model: " + modelType + " => '" + modelName + "'");
				this.getApsApi().getShareApi().setShareInfo(model.getId().toString(), changeRequest);
			}
		}
	}
	
	private void analyzeChanges(String modelName, Set<String> readers, Set<String> editors, SharePermission share, ShareInfoRequest changeRequest) {
		if (PermissionLevel.Write.equals(share.getPermission())) {
			if (editors.contains(share.getGroup().getName())) {
				this.getLog().debug("The named group '" + share.getGroup().getName() + "' is already an editor of model '" + modelName + "'");
				// no change
				return;
			} else if (readers.contains(share.getGroup().getName())) {
				this.getLog().debug("The named group '" + share.getGroup().getName() + "' reverting from editor to reader of model '" + modelName + "'");
				changeRequest.getUpdated().add(new PermissionLight().withId(share.getId()).withPermission(PermissionLevel.Read));
				return;
			}
		} else {
			if (editors.contains(share.getGroup().getName())) {
				this.getLog().debug("The named group '" + share.getGroup().getName() + "' elevating from reader to editor of model '" + modelName + "'");
				changeRequest.getUpdated().add(new PermissionLight().withId(share.getId()).withPermission(PermissionLevel.Write));
				return;
			} else if (readers.contains(share.getGroup().getName())) {
				this.getLog().debug("The named group '" + share.getGroup().getName() + "' is already an reader of model '" + modelName + "'");
				// no change
				return;
			}
		}
		
		if (this.doRevoke) {
			this.getLog().debug("The named group '" + share.getGroup().getName() + "' is an unregulated editor of model '" + modelName + "'; revoking ...");
			changeRequest.getRemoved().add(new PermissionLight().withId(share.getId()));
		} else {
			this.getLog().debug("The named group '" + share.getGroup().getName() + "' is an unregulated editor of model '" + modelName + "'");
			// not touching extra unnamed permissions
		}
	}
	
	private void analyzeChanges(String modelName, String groupName, PermissionLevel plevel, ShareInfoRequest changeRequest) {
		Long groupId = this.identityIndex.getValue(groupName);
		if (groupId == null) {
			this.getLog().warn("The named group '" + groupName + "' does not exist in APS; ignoring ...");
		} else {
			this.getLog().debug("The named group '" + groupName + "' granted the " + plevel + " role of model '" + modelName + "'");
			changeRequest.getAdded().add(new PermissionLight().withGroupId(groupId).withPermission(plevel));
		}
	}
	
	protected void normalizeParameters() {
		Set<String> readerSet = this.normalizeParameter(this.readers);
		Set<String> editorSet = this.normalizeParameter(this.editors);
		this.appReaderSet = this.normalizeParameter(this.appReaders, readerSet);
		this.appEditorSet = this.normalizeParameter(this.appEditors, editorSet);
		this.processReaderSet = this.normalizeParameter(this.processReaders, readerSet);
		this.processEditorSet = this.normalizeParameter(this.processEditors, editorSet);
		this.formReaderSet = this.normalizeParameter(this.formReaders, readerSet);
		this.formEditorSet = this.normalizeParameter(this.formEditors, editorSet);
	}
	
	private Set<String> normalizeParameter(String parameter, Collection<String> c) {
		Set<String> set = this.normalizeParameter(parameter);
		set.addAll(c);
		return set;
	}
	
	private Set<String> normalizeParameter(String parameter) {
		Set<String> params = new HashSet<>();
		if (parameter == null)
			return params;
		if (parameter.length() == 0)
			return params;
		
		String[] splitParams = parameter.split(",");
		params.addAll(Arrays.asList(splitParams));
		return params;
	}
	
	protected void buildIdentityIndex() throws MojoExecutionException {
		List<Tenant> tenants = this.getApsApi().getAdminApi().getTenants();
		for (Tenant tenant : tenants) {
			List<GroupLight> groups = this.getApsApi().getAdminApi().getGroups(tenant.getId(), true, true);
			this.getLog().debug("Indexing groups: " + groups.size());
			for (GroupLight group : groups)
				this.identityIndex.put(group.getName(), group.getId());
			if (this.getLog().isDebugEnabled())
				this.getLog().debug("Indexed groups: " + this.identityIndex.toString());
		}
	}

}

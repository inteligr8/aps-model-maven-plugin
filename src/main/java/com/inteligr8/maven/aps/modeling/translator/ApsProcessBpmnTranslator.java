/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.inteligr8.alfresco.activiti.ApsPublicRestApi;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.maven.aps.modeling.util.Index;
import com.inteligr8.maven.aps.modeling.util.ModelUtil;

/**
 * This class implements an APS Process BPMN/XML configuration file translator.
 * 
 * This will translate the IDs of all the APS Process, Organizations, and
 * Forms referenced in the APS Process BPMN.
 * 
 * Caution: in lieu of implementing APS User translation, APS User identity
 * references are simply removed.  Using APS Users in your APS models is bad
 * practice.  Use APS Organizations, even if that user is the only member of
 * the group.
 * 
 * @author brian@inteligr8.com
 */
public class ApsProcessBpmnTranslator extends ApsOrganizationHandler implements ApsFileTranslator {

	private static final String NAMESPACE_ACTIVITI = "http://activiti.org/bpmn";
	private static final String NAMESPACE_ACTIVITI_MODELER = "http://activiti.com/modeler";
	
	private final Logger logger = LoggerFactory.getLogger(ApsProcessBpmnTranslator.class);
	private final Index<Long, String> apsIndex;
	private final Map<String, GroupLight> apsOrgIndex;
	private final Index<Long, String> apsFormIndex;
	private final Index<Long, String> fileFormIndex;
	
	/**
	 * This constructor initializes the default translator.
	 * 
	 * @param api An APS API reference.
	 * @param apsProcessIndex A map of process IDs to process names as defined in the APS Service.
	 * @param apsOrgIndex A map of organizations (groups) to organization meta-data as defined in the APS Service.
	 * @param apsFormIndex A map of form IDs to form names as defined in the APS Service.
	 * @param fileFormIndex A map of form IDS to form names as used in the local form files.
	 */
	public ApsProcessBpmnTranslator(
			ApsPublicRestApi api,
			Index<Long, String> apsProcessIndex,
			Map<String, GroupLight> apsOrgIndex,
			Index<Long, String> apsFormIndex,
			Index<Long, String> fileFormIndex) {
		super(api, apsOrgIndex);
		this.apsIndex = apsProcessIndex;
		this.apsOrgIndex = apsOrgIndex;
		this.apsFormIndex = apsFormIndex;
		this.fileFormIndex = fileFormIndex;
	}
	
	@Override
	public void translateFile(File file, String processName, Long processId) throws IOException {
		this.logger.debug("Translating BPMN file: {}", file);
		
		boolean changed = false;
		Long apsProcessId = this.apsIndex.getFirstKey(processName);
		
		try {
			Document bpmn = ModelUtil.getInstance().readXml(file);
			Element definitionsElement = bpmn.getDocumentElement();
			
			changed = this.translateRootElement(definitionsElement, apsProcessId) || changed;
	
			NodeList candidateGroupElements = ModelUtil.getInstance().xpath(definitionsElement, "//*[@activiti:candidateGroups]");
			for (int n = 0; n < candidateGroupElements.getLength(); n++) {
				Element candidateGroupElement = (Element)candidateGroupElements.item(n);
				changed = this.translateTaskCandidateGroup(candidateGroupElement) || changed;
			}
	
			NodeList formKeyElements = ModelUtil.getInstance().xpath(definitionsElement, "//*[@activiti:formKey]");
			for (int n = 0; n < formKeyElements.getLength(); n++) {
				Element formKeyElement = (Element)formKeyElements.item(n);
				changed = this.translateTaskForm(formKeyElement) || changed;
			}
	
			NodeList subprocessElements = ModelUtil.getInstance().xpath(definitionsElement, "//tns:subProcess");
			for (int n = 0; n < subprocessElements.getLength(); n++) {
				Element subprocessElement = (Element)subprocessElements.item(n);
				changed = this.translateSubprocess(subprocessElement) || changed;
			}
	
			NodeList conditionDefinitionElements = ModelUtil.getInstance().xpath(definitionsElement, "//modeler:conditionDefinition");
			for (int n = 0; n < conditionDefinitionElements.getLength(); n++) {
				Element conditionDefinitionElement = (Element)conditionDefinitionElements.item(n);
				changed = this.translateConditionDefinition(conditionDefinitionElement) || changed;
			}
	
			NodeList conditionExpressions = ModelUtil.getInstance().xpath(definitionsElement, "//tns:conditionExpression/text()");
			for (int n = 0; n < conditionExpressions.getLength(); n++) {
				CharacterData conditionExpression = (CharacterData)conditionExpressions.item(n);
				changed = this.translateConditionExpression(conditionExpression) || changed;
			}
			
			if (changed)
				ModelUtil.getInstance().writeXml(bpmn, file);
		} catch (TransformerException | XPathExpressionException | SAXException e) {
			throw new IllegalStateException("An XML issue occurred", e);
		}
	}
	
	private boolean translateRootElement(Element definitionsElement, Long apsProcessId) {
		boolean changed = false;
		
		if (apsProcessId != null)
			changed = this.setAttributeIfSet(definitionsElement, NAMESPACE_ACTIVITI_MODELER, "modelId", apsProcessId) || changed;
		
		return changed;
	}
	
	
	
	private boolean translateTaskCandidateGroup(Element taskElement) throws XPathExpressionException {
		this.logger.trace("Translating task for candidate groups: {}", taskElement.getAttribute("id"));
		boolean changed = false;
		
		Attr candidateGroupsAttr = taskElement.getAttributeNodeNS(NAMESPACE_ACTIVITI, "candidateGroups");
		if (candidateGroupsAttr == null)
			return false;

		this.logger.trace("Found '{}' candidate groups in the APS Process BPMN model", candidateGroupsAttr.getValue());
		
		List<Long> groupIdsList = new LinkedList<>();
		
		String[] groupIds = candidateGroupsAttr.getValue().split(",");
		for (String groupIdStr : groupIds) {
			groupIdStr = groupIdStr.trim();
			if (groupIdStr.length() == 0)
				continue;
			long orgId = Long.parseLong(groupIdStr.trim());
			
			Element groupNameElement = (Element)ModelUtil.getInstance().xpath(
					taskElement,
					"tns:extensionElements/modeler:group-info-name-" + orgId,
					XPathConstants.NODE);
			if (groupNameElement == null)
				throw new IllegalStateException("The candidate group ID " + orgId +" does not have a corresponding candidate group name");

			String groupName = groupNameElement.getTextContent().trim();
			this.logger.trace("Found '{}' candidate group in the APS Process BPMN model", groupName);
			
			GroupLight apsOrg = this.apsOrgIndex.get(groupName);
			long apsOrgId = 0L;
			if (apsOrg == null) {
				this.logger.debug("The organization '{}' does not exist in APS; creating it ...", groupName);
				apsOrgId = this.createOrganization(groupName);
			} else {
				apsOrgId = apsOrg.getId();
			}
			
			if (apsOrgId != orgId) {
				this.logger.debug("The organization '{}' exists in APS with ID {}; changing model", groupName, apsOrgId);
				groupNameElement = (Element)groupNameElement.getOwnerDocument().renameNode(groupNameElement,
						groupNameElement.getNamespaceURI(), "modeler:group-info-name-" + apsOrgId);

				groupIdsList.add(apsOrgId);
				changed = true;
			} else {
				this.logger.trace("The organization '{}' ID does not change; leaving unchanged", groupName);
				
				groupIdsList.add(apsOrgId);
			}
		}
		
		if (changed)
			candidateGroupsAttr.setValue(StringUtils.join(groupIdsList, ","));
		return changed;
	}
	
	private boolean translateTaskForm(Element taskElement) throws XPathExpressionException {
		this.logger.trace("Translating task for forms: {}", taskElement.getAttribute("id"));
		boolean changed = false;

		String formRefName = (String)ModelUtil.getInstance().xpath(
				taskElement,
				"tns:extensionElements/modeler:form-reference-name/text()",
				XPathConstants.STRING);
		if (formRefName == null)
			throw new IllegalStateException("A form was detected in the task, but no form name was found: " + taskElement.getAttribute("id"));
		
		formRefName = formRefName.trim();
		this.logger.trace("Found '{}' form in the APS Process BPMN model", formRefName);
		
		Long apsFormId = this.apsFormIndex.getFirstKey(formRefName);
		if (apsFormId == null) {
			this.logger.debug("The form '{}' does not exist in APS; leaving unchanged", formRefName);
			return false;
		}

		this.logger.trace("Found ID {} for the form '{}' in the APS Process BPMN model", apsFormId, formRefName);
		
		Attr formKeyAttr = taskElement.getAttributeNodeNS(NAMESPACE_ACTIVITI, "formKey");
		if (formKeyAttr == null)
			throw new IllegalStateException("A form was detected in the task, but no form key was found: " + taskElement.getAttribute("id"));
		
		long formKey = Long.parseLong(formKeyAttr.getValue().trim());
		if (apsFormId != formKey) {
			this.logger.debug("The form '{}' exists in APS with ID {}; changing model", formRefName, apsFormId);
			formKeyAttr.setValue(apsFormId.toString());
			changed = true;
		} else {
			this.logger.trace("The form '{}' key does not change; leaving unchanged", formRefName);
		}
		
		CharacterData formRefIdData = (CharacterData)ModelUtil.getInstance().xpath(
				taskElement,
				"tns:extensionElements/modeler:form-reference-id",
				ModelUtil.CDATA_FLEX);
		if (formRefIdData == null)
			throw new IllegalStateException("A form was detected in the task, but no form reference ID was found: " + taskElement.getAttribute("id"));
		
		long formRefId = Long.parseLong(formRefIdData.getData());
		if (apsFormId != formRefId) {
			this.logger.debug("The form '{}' reference exists in APS with ID {}; changing model", formRefName, apsFormId);
			formRefIdData.setData(apsFormId.toString());
			changed = true;
		} else {
			this.logger.trace("The form '{}' reference ID does not change; leaving unchanged", formRefName);
		}
		
		return changed;
	}
	
	private boolean translateSubprocess(Element subprocessElement) throws XPathExpressionException {
		this.logger.trace("Translating subprocess: {}", subprocessElement.getAttribute("id"));
		boolean changed = false;

		String subprocessName = (String)ModelUtil.getInstance().xpath(
				subprocessElement,
				"tns:extensionElements/modeler:subprocess-name/text()",
				XPathConstants.STRING);
		if (subprocessName == null) {
			this.logger.warn("The subprocess task '{}' has no defined subprocess; ignoring", subprocessElement.getAttribute("id"));
			return false;
		}
		
		subprocessName = subprocessName.trim();
		this.logger.trace("Found '{}' subprocess in the APS Process BPMN model", subprocessName);
		
		Long apsProcessId = this.apsIndex.getFirstKey(subprocessName);
		
		if (apsProcessId == null) {
			this.logger.debug("The process '{}' does not exist in APS; leaving unchanged", subprocessName);
			return false;
		}

		this.logger.trace("Found ID {} for the subprocess '{}' in the APS Process BPMN model", apsProcessId, subprocessName);
		
		CharacterData subprocessIdData = (CharacterData)ModelUtil.getInstance().xpath(
				subprocessElement,
				"tns:extensionElements/modeler:subprocess-id",
				ModelUtil.CDATA_FLEX);
		long subprocessId = Long.parseLong(subprocessIdData.getData());
		if (apsProcessId != subprocessId) {
			this.logger.debug("The process '{}' exists in APS with ID {}; changing model", subprocessName, apsProcessId);
			subprocessIdData.setData(apsProcessId.toString());
			changed = true;
		} else {
			this.logger.trace("The process '{}' ID does not change; leaving unchanged", subprocessName);
		}
		
		return changed;
	}
	
	private boolean translateConditionDefinition(Element conditionDefinitionElement) throws XPathExpressionException {
		this.logger.trace("Translating condition definition for outcome form elements: {}", conditionDefinitionElement.getParentNode().getParentNode().getAttributes().getNamedItem("id"));
		boolean changed = false;

		Attr outcomeFormNameAttr = conditionDefinitionElement.getAttributeNodeNS(NAMESPACE_ACTIVITI_MODELER, "outcomeFormName");
		if (outcomeFormNameAttr != null) {
			String outcomeFormName = outcomeFormNameAttr.getValue();
			Long outcomeApsFormId = this.apsFormIndex.getFirstKey(outcomeFormName);
			if (outcomeApsFormId != null) {
				Attr outcomeFormIdAttr = conditionDefinitionElement.getAttributeNodeNS(NAMESPACE_ACTIVITI_MODELER, "outcomeFormId");
				long outcomeFormId = Long.parseLong(outcomeFormIdAttr.getValue().trim());
				if (outcomeApsFormId != outcomeFormId) {
					this.logger.debug("The form '{}' exists in APS with ID {}; leaving unchanged", outcomeFormName, outcomeApsFormId);
					outcomeFormIdAttr.setValue(String.valueOf(outcomeApsFormId));
					changed = true;
				} else {
					this.logger.trace("The form '{}' ID does not change; leaving unchanged", outcomeFormName);
				}
			}
		}
		
		return changed;
	}
	
	private boolean translateConditionExpression(CharacterData conditionExpressionCdata) throws XPathExpressionException {
		this.logger.trace("Translating condition expression for outcome form elements: {}", conditionExpressionCdata.getParentNode().getParentNode().getAttributes().getNamedItem("id"));
		boolean changed = false;

		String conditionExpression = conditionExpressionCdata.getData();
		StringBuilder newce = new StringBuilder();
		int pos = 0;
		
		Pattern formOutcomeInExpressionPattern = Pattern.compile("form([0-9]+)outcome");
		Matcher matcher = formOutcomeInExpressionPattern.matcher(conditionExpression);
		while (matcher.find()) {
			newce.append(conditionExpression.substring(pos, matcher.start(1)));
			
			long formId = Long.parseLong(matcher.group(1));
			String formName = this.fileFormIndex.getValue(formId);
			if (formName == null) {
				this.logger.warn("The form ID '{}' does not exist in the form model files; ignoring", formId);
				newce.append(formId);
			} else {
				Long apsFormId = this.apsFormIndex.getFirstKey(formName);
				if (apsFormId == null) {
					this.logger.warn("The form '{}' does not exist in APS; leaving unchanged", formName);
					newce.append(formId);
				} else {
					newce.append(apsFormId);
					changed = true;
				}
			}
			
			pos = matcher.end(1);
		}

		newce.append(conditionExpression.substring(pos));
		
		if (changed)
			conditionExpressionCdata.setData(newce.toString());
		
		return changed;
	}
	

	
	private boolean setAttributeIfSet(Element element, String attrNamespace, String attrName, Object value) {
		Attr attr = element.getAttributeNodeNS(attrNamespace, attrName);
		if (attr == null)
			return false;
		
		attr.setValue(value.toString());
		return true;
	}

}

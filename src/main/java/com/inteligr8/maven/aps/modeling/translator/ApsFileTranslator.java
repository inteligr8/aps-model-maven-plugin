/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.translator;

import java.io.File;
import java.io.IOException;

import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;

/**
 * This interface defines an APS file translator.
 * 
 * Translation is the transformation of something from one state to a target
 * state.  In this context, a configuration is being translated to be import
 * friendly for the target APS Service.  That principally means the update
 * of APS model IDs.
 */
public interface ApsFileTranslator extends ApsFileTransformer {
	
	@Override
	default void transformFile(File file, String modelName, Long modelId) throws IOException {
		this.translateFile(file, modelName, modelId);
	}
	
	/**
	 * This method translates the specified file which should be referred to
	 * with the specified name and ID.
	 * 
	 * @param file A model file.
	 * @param modelName The target model name.
	 * @param modelId The target model ID.
	 * @throws IOException An I/O related exception has occurred during translation.
	 */
	void translateFile(File file, String modelName, Long modelId) throws IOException;

}

/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;

import com.inteligr8.maven.aps.modeling.crawler.ApsFileTransformer;

/**
 * This interface defines an APS file normalizer.
 * 
 * Normalization is the transformation of something from one state to a target
 * (normalized) state.  In this context, a configuration is being normalized to
 * be diff-friendly.  That principally means the removal of dates.  In theory,
 * two normalized files will always match unless something of real substance
 * has changed.
 */
public interface ApsFileNormalizer extends ApsFileTransformer {
	
	@Override
	default void transformFile(File file, String modelName, Long modelId) throws IOException {
		this.normalizeFile(file, modelName);
	}
	
	/**
	 * This method normalizes the specified file which should be referred to
	 * with the specified name.
	 * 
	 * @param file A model file.
	 * @param modelName The model name.
	 * @throws IOException An I/O related exception has occurred during normalization.
	 */
	void normalizeFile(File file, String modelName) throws IOException;

}

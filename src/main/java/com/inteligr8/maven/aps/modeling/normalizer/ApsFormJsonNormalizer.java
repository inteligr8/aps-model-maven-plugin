/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.normalizer;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements an APS Form JSON configuration file normalizer.
 * 
 * This does nothing but log at this time.
 * 
 * @author brian@inteligr8.com
 */
public class ApsFormJsonNormalizer implements ApsFileNormalizer {

	private final Logger logger = LoggerFactory.getLogger(ApsFormJsonNormalizer.class);
	
	@Override
	public void normalizeFile(File file, String modelName) throws IOException {
		this.logger.debug("Normalizing Form JSON file: {}", file);
		this.logger.trace("Nothing to normalize: {}", modelName);
	}

}

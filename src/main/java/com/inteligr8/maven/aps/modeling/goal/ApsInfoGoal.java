/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.goal;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.codehaus.plexus.component.annotations.Component;

import com.inteligr8.alfresco.activiti.model.AppVersion;

/**
 * A class that implements an APS service information goal.
 * 
 * This goal will simply output the APS edition and version to the Maven output
 * at the `INFO` level.  It is a great way to test your connection
 * configuration and network accessibility of the service.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "aps-info", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class ApsInfoGoal extends ApsAddressibleGoal {
	
	@Override
	public void executeEnabled() throws MojoExecutionException, MojoFailureException {
		AppVersion version = this.getApsApi()
				.getAppVersionApi()
				.get();
		
		this.getLog().debug("APS type: " + version.getType());
		this.getLog().info("APS edition: " + version.getEdition());
		this.getLog().info("APS version: " + version.getMajorVersion() + "." + version.getMinorVersion() + "." + version.getRevisionVersion());
	}

}

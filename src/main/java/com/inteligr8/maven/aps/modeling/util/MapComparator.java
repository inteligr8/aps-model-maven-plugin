/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.aps.modeling.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * This class implements a comparator for the value of map keys.
 * 
 * If key values are considered equal, fallback keys are supported.  Any map
 * without the key or not null value will be considered "greater than" ones
 * that have values for the specified key.  This will move them to the end of
 * any sorted list.
 * 
 * @author brian@inteligr8.com
 * @param <K> An map key type.
 * @param <V> An map value type.
 */
public class MapComparator<K, V> implements Comparator<Map<K, V>> {
	
	//private final Logger logger = LoggerFactory.getLogger(ArrayOfObjectNodeComparator.class);
	private final List<K> keys;
	
	/**
	 * An array-based constructor.
	 * 
	 * The class will sort on the values of these map keys, with a priority in
	 * the specified order.
	 * 
	 * @param keys An array of keys.
	 */
	public MapComparator(@SuppressWarnings("unchecked") K... keys) {
		this(Arrays.asList(keys));
	}
	
	/**
	 * A list-based constructor.
	 * 
	 * The class will sort on the values of these map keys, with a priority in
	 * the specified order.
	 * 
	 * @param keys A list of keys.
	 */
	public MapComparator(List<K> keys) {
		this.keys = keys;
	}
	
	@Override
	public int compare(Map<K, V> m1, Map<K, V> m2) {
		for (K key : keys) {
			V value1 = m1.get(key);
			V value2 = m2.get(key);
			
			if (value1 == null && value2 == null)
				continue;
			
			if (value1 == null)
				return 1;
			if (value2 == null)
				return -1;
			if (!value1.getClass().equals(value2.getClass()))
				return 0;
			
			if (value1 instanceof Boolean) {
				return Boolean.compare((Boolean)value1, (Boolean)value2);
			} if (value1 instanceof Number) {
				return Double.compare(((Number)value1).doubleValue(), ((Number)value2).doubleValue());
			} if (value1 instanceof String) {
				return ((String)value1).compareTo((String)value2);
			}
		}
		
		return 0;
	}

}

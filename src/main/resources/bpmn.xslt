<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes" />
  <xsl:strip-space elements="*"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="text()">
  	<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
  	<xsl:value-of select="." disable-output-escaping="yes" />
  	<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
  </xsl:template>
</xsl:stylesheet>

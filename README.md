
# APS Model Maven Plugin

This is a maven plugin that APS model management.  The use cases include synchronization between disparate APS instances, like the progressive environments you find in most enterprise infrastructures.  It will also synchronize APS instances to Git or Git to APS instances.

## Usage

Here is an example configuration for synchronizing an APS instance to the local project so it can be committed to Git.

```xml
<project>
    ...
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>com.inteligr8</groupId>
                <artifactId>aps-model-maven-plugin</artifactId>
                <version>...</version>
                <configuration>
                    <activitiAppBaseUrl>http://localhost:8080/activiti-app</activitiAppBaseUrl>
                    <activitiAppAuthBasicServerId>aps</activitiAppAuthBasicServerId>
                    <apsAppName>Test App</apsAppName>
                </configuration>
                <executions>
                    <execution>
                        <id>download-unpack-app</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>download-app</goal>
                            <goal>unpack-app</goal>
                        </goals>
                        <configuration>
                            <normalize>true</normalize>
                            <diffFriendly>true</diffFriendly>
                        </configuration>
                    </execution>
                    <execution>
                        <id>translate-app</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>translate-app</goal>
                        </goals>
                        <configuration>
                            <finalDirectory>${basedir}/src/main/aps/app</finalDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
    ...
</project>
```

For `BASIC` authentication, you will need to define a `server` in the `settings.xml` as shown in the snippet below.

```xml
<settings ...>
    ...
    <servers>
        ...
        <server>
            <id>aps</id>
            <username>...</username>
            <password>...</password>
        </server>
        ...
    </servers>
    ...
</settings>
```

Here is an example configuration for synchronizing the local project to an APS instance.

```xml
<project>
    ...
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>com.inteligr8</groupId>
                <artifactId>aps-model-maven-plugin</artifactId>
                <version>...</version>
                <configuration>
                    <activitiAppBaseUrl>http://localhost:8080/activiti-app</activitiAppBaseUrl>
                    <activitiAppAuthBasicServerId>aps</activitiAppAuthBasicServerId>
                    <apsAppName>Test App</apsAppName>
                </configuration>
                <executions>
                    <execution>
                        <id>translate-app</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>translate-app</goal>
                        </goals>
                        <configuration>
                            <unzipDirectory>${basedir}/src/main/aps/app</unzipDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>pack-app</id>
                        <phase>package</phase>
                        <goals>
                            <goal>pack-app</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>upload-app</id>
                        <phase>install</phase>
                        <goals>
                            <goal>upload-app</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>publish-app</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>publish-app</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
    ...
</project>
```

Here is an example configuration for synchronizing directly from a source to different target APS instance.

```xml
<project>
    ...
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>com.inteligr8</groupId>
                <artifactId>aps-model-maven-plugin</artifactId>
                <version>...</version>
                <configuration>
                    <apsAppName>Test App</apsAppName>
                </configuration>
                <executions>
                    <execution>
                        <id>download-unpack-app</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>download-app</goal>
                            <goal>unpack-app</goal>
                        </goals>
                        <configuration>
                            <activitiAppBaseUrl>http://dev-host:8080/activiti-app</activitiAppBaseUrl>
                            <activitiAppAuthBasicServerId>aps-dev</activitiAppAuthBasicServerId>
                            <normalize>true</normalize>
                            <diffFriendly>true</diffFriendly>
                        </configuration>
                    </execution>
                    <execution>
                        <id>translate-app</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>translate-app</goal>
                        </goals>
                        <configuration>
                            <activitiAppBaseUrl>http://test-host:8080/activiti-app</activitiAppBaseUrl>
                            <activitiAppAuthBasicServerId>aps-test</activitiAppAuthBasicServerId>
                        </configuration>
                    </execution>
                    <execution>
                        <id>pack-app</id>
                        <phase>package</phase>
                        <goals>
                            <goal>pack-app</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>upload-publish-app</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>upload-app</goal>
                            <goal>publish-app</goal>
                        </goals>
                        <configuration>
                            <activitiAppBaseUrl>http://test-host:8080/activiti-app</activitiAppBaseUrl>
                            <activitiAppAuthBasicServerId>aps-test</activitiAppAuthBasicServerId>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
    ...
</project>
```

You will want the Maven project to be of packaging type `pom`.

## Goals

| Goal            | Description |
| --------------- | ----------- |
| `aps-info`      | Check the connection to APS and output version information. |
| `download-app`  | Export and download an APS App as a ZIP file from an APS instance.  The latest published version is exported. |
| `upload-app`    | Upload and import an APS App from a ZIP file to an APS instance.  The APS App is created or updated, but not published. |
| `unpack-app`    | Unzip the ZIP file exported from the APS instance. |
| `pack-app`      | Zip a previously unpacked APS App in preparation for import to an APS instance. |
| `publish-app`   | Publish an existing APS App in an APS instance. |
| `translate-app` | Translate an unpacked APS App. |
| `share-models`  | Share applicable models in an APS instance. |

### Goal: `aps-info`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `download-app`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `zipDirectory`                 | `string`  |          | `target/aps` | The base directory where the ZIP file should be downloaded. |
| `apsAppName`                   | `string`  | Yes      |         | An APS App name that exists in the APS instance. |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `unpack-app`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `apsAppName`                   | `string`  | Yes      |         | The name of the APS App ZIP file, without the `.zip` extension. |
| `zipDirectory`                 | `string`  |          | `target/aps` | The base directory where the ZIP file should exist. |
| `unzipDirectory`               | `string`  |          | `target/aps/app` | The base directory where the ZIP file should be unpacked; the APS App name will be another folder within this directory. |
| `reformat`                     | `boolean` |          | `true`  | The JSON and XML files should be reformatted for readability and better `diff`/Git support. |
| `normalize`                    | `boolean` | Yes      |         | The JSON and XML files should be normalized for better `diff`/Git support. |
| `diffFriendly`                 | `boolean` | Yes      |         | The JSON file models should be sorted for better `diff`/Git support. |
| `charsetName`                  | `string`  |          | `utf-8` | |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `translate-app`

This goal translates an APS App configuration so all the model and organization IDs reference the specified APS instance.  This is the most important functionality provied by this Maven plugin.  It allows for the clean synchronization of APS models between environments; forward and backwards.

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `apsAppName`                   | `string`  | Yes      |         | The name of the folder in the `unzipDirectory`. |
| `unzipDirectory`               | `string`  |          | `target/aps/app` | The base directory where the unpacked APS App exists. |
| `finalDirectory`               | `string`  |          | `target/aps/app` | The base directory where the translated APS App should exist.  If it is the same as `unzipDirectory`, then it is translated in-place. |
| `overwrite`                    | `boolean` |          | `true`  | |
| `charsetName`                  | `string`  |          | `utf-8` | |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `pack-app`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `apsAppName`                   | `string`  | Yes      |         | The name of the folder in the `unzipDirectory`. |
| `unzipDirectory`               | `string`  |          | `target/aps/app` | The base directory where the unpacked APS App is located; the APS App name will be a folder within this directory. |
| `zipDirectory`                 | `string`  |          | `target/aps` | The base directory where the new ZIP file should exist. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `upload-app`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `zipDirectory`                 | `string`  |          | `target/aps` | The base directory where the ZIP file should be downloaded. |
| `apsAppName`                   | `string`  | Yes      |         | An APS App name that exists in the APS instance. |
| `publish`                      | `boolean` |          | `false` | Automatically publish after upload. |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `publish-app`

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `apsAppName`                   | `string`  | Yes      |         | An APS App name that exists in the APS instance. |
| `comment`                      | `string`  |          | `Automated by 'aps-model-maven-plugin'` | A comment to apply to the new version of the APS App. |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

### Goal: `share-models`

This goal queries the APS instance for all applicable models and shares them based on the configuration.  This is very useful in highly collaborative environments.  It may be worth having this run on a routine basis in the development environment and on every synchronization to other environments.

| Configuration Property         | Data Type | Required | Default | Description |
| ------------------------------ |:---------:|:--------:| ------- | ----------- |
| `modelName`                    | `string`  |          |         | A single model to share.  If the same name is used for two different model types (app and process), then both are shared.  If this is left unspecified, all models are shared. |
| `readers`                      | `string`  |          |         | A comma-delimited list of organizations/groups to target for readonly access to all model types. |
| `editors`                      | `string`  |          |         | A comma-delimited list of organizations/groups to target for editor access to all model types. |
| `appReaders`                   | `string`  |          |         | A comma-delimited list of organizations/groups to target for readonly access to only APS App models.  This is needed for APS App publishers. |
| `appEditors`                   | `string`  |          |         | A comma-delimited list of organizations/groups to target for editor access to only APS App models.  This is needed for APS App collators. |
| `processReaders`               | `string`  |          |         | A comma-delimited list of organizations/groups to target for readonly access to only APS Process models.  This is needed for APS App collators. |
| `processEditors`               | `string`  |          |         | A comma-delimited list of organizations/groups to target for editor access to only APS Process models.  This is needed for APS Process modelers. |
| `formReaders`                  | `string`  |          |         | A comma-delimited list of organizations/groups to target for readonly access to only APS Form models.  This is needed for APS Process modelers. |
| `formEditors`                  | `string`  |          |         | A comma-delimited list of organizations/groups to target for editor access to only APS Form models.  This is needed for APS Form designers. |
| `doRevoke`                     | `boolean` |          | `false` | If `true`, revoke permission to any other groups/users for the applicable models; `false` will leave those permissions intact. |
| `activitiAppBaseUrl`           | `string`  |          | `http://localhost:8080/activiti-app` | The base URL of the APS instance. |
| `activitiAppAuthType`          | `string`  |          | `BASIC` | Either `BASIC` or `OAuth`; case insensitive. |
| `activitiAppAuthBasicServerId` | `string`  |          | `aps`   | The `settings.xml` server ID that provides the credentials for the APS instance when using `BASIC` authentication. |
| `oauthCode`                    | `string`  | *Maybe*  |         | An authorization code for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthClientServerId`          | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the client ID/secret credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthServerId`                | `string`  | *Maybe*  |         | The `settings.xml` server ID that provides the username/password credentials for the identity provider governing the APS instance when using `OAuth` authentication. |
| `oauthTokenUrl`                | `string`  | *Maybe*  |         | The token URL for the identity provider governing the APS instance when using `OAuth` authentication. |
| `skip`                         | `boolean` |          | `false` | |

